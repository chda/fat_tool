import numpy as np
import matplotlib.pyplot as plt
# from labellines import labelLines
import lookup

def forest_growth(elevation, forest_type ):
    """
    forest_type  1 = bushes, 2 = mixed forest, 3 = evergreen

    UPDATE FROM GEORG KINDERMANN following Kindermann (2018) and update with Data from the Austrian National Forest Inventory (ANFI 1981-2019)
    """

    if forest_type == 3:
        c0 = lookup.evergreen(elevation)
        # c0 = #value taken from forest raster Austrian National Forest Inventory (ANFI 1981-2019)
        #Forest type conifer forest (spruce):
        hkc1 = 0.75
        hkc3 =  3
        hkcc0 = -11.14612447
        hkcc1 = 0.02023767
        hkcc2 = 1.90310459

        top_height = c0 * log(1 + exp(hkcc0 + hkcc1 * c0 ^ hkcc2) * age ^ hkc3) ^ hkc1


    elif forest_type == 2: # leaf trees
        #Forest type broad-leaved forest (beech)
        c0 = lookup.leafy(elevation)

        hkc1 = 0.75
        hkc3 = 3
        hkcc0= -12
        hkcc1 = 5.071840
        hkcc2 = 27.419350
        hkcc3 = -2.830829


        top_height = c0 * log(1 + exp(hkcc0 + hkcc1 / (1 + exp(hkcc2 + hkcc3 * c0))) * age ^ hkc3) ^ hkc1

    else:
        top_height = 1.5 # bushes hieght

    return top_height

if __name__ == "__main__":
    #elevation vs oh100(tree size)

    forest_type = 3 #  1 = bushes, 2 = mixed forest, 3 = evergreen
    elevation = range(0,2100, 100)
    slope = (37.3- 14)/ -2000
    OH100 =[]
    alt_OH100 = []
    alt_slope = (14-37.3)/(2000-500)
    fig, ax = plt.subplots()
    ax.set_xlabel('time [years]')
    ax.set_ylabel('tree height [m]')
    ax.set_title('Forest growth')
    ax.axvline(x=25, color = 'red')
    ax.axvline(x=50, color = 'red')
    ax.axvline(x=100, color = 'red')


    for height in elevation:
        oh100 = (height*slope + 37.3)
        alt_oh100 =  min((height*alt_slope + (37.3-500* alt_slope)), 37.3)
        years, tree_height = forest_growth(oh100, forest_type)
        OH100.append(oh100)
        alt_OH100.append(alt_oh100)
        #ax.plot(years, tree_height, label='HO100 = %s , at %s m ABSL '%(oh100, height), color='gray')
        ax.plot(years, tree_height, label='At %s m ABSL ' % (height))
        """
        '%s %s %s'%('python','is','fun')
    #oh14 = 14
    #years, tree_height = forest_growth(oh14, evergreen = True)


    ############ plot ##################



    #ax.plot(years, tree_height, label='HO100 = 14 2000 masl', color='black')

    oh37 = 37.3
    years, tree_height = forest_growth(oh37, evergreen = True)
    ax.plot(years, tree_height, label='HO100 = 37.3, 0 masl', color='black')
    # ax.plot(slope_angle, forest_reduction_function, label='Prob reduction function', color='black')

    oh30 = 30
    years, tree_height = forest_growth(oh30, evergreen = True)
    ax.plot(years, tree_height, label='HO100 = 30, ', linestyle ='--')


    #years, tree_height = forest_growth(19.6, evergreen = False)
    #ax.plot(years, tree_height, label='HO100 = 19.6 bubra', color='blue', linestyle = "--")

    #years, tree_height = forest_growth(35.4, evergreen = False)
    #ax.plot(years, tree_height, label='HO100 = 35.4 bubra', color='green', linestyle = "--")
    """
    xvals = np.ones_like(OH100)*149 # x location of label lines
    labelLines(plt.gca().get_lines(), zorder=.5, xvals=xvals, align= False, backgroundcolor='none')
    #fig.legend(bbox_to_anchor=(0.9, 0.83))
    ax.grid(True, axis="both", which="major")
    ax.grid(True, axis="both", which="minor", linestyle=":")
    ax.set_xlim(0,170)

######################### figure 2####################

    fig1, ax1 = plt.subplots()
    ax1.set_xlabel('elevation above sea level [m]')
    ax1.set_ylabel('Evergreen OH100 [m]')
    ax1.set_title('Tree size vs elevation (evergreen)')
    ax1.plot(elevation, OH100, label='Linear', color='black')
    ax1.plot(elevation, alt_OH100, label='Piecewise linear ', color='green')
    fig1.legend(bbox_to_anchor=(0.9, 0.83))
    ax1.grid(True, axis="both", which="major")
    ax1.grid(True, axis="both", which="minor", linestyle=":")
    ax1.set_ylim(0,40)
    plt.minorticks_on()

    plt.show()