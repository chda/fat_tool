import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import math


def soil():
    """
    This routine was made from the paper:
    The influence of forest cover on landslide occurrence explored with spatio-temporal information
    by: Elmar M. Schmaltz, Stefan Steger, Thomas Glade
    """
    slope_angle = [0, 2.5, 5, 7.5, 10, 12.5, 15, 17.5, 20, 22.5, 25, 27.5, 30, 32.5, 35, 37.5, 40, 42.5, 45, 47.5, 50,
                   52.5, 55, 57.5, 60, 62.5, 65, 67.5, 70, 72.5, 75, 77.5, 80, 82.5, 85, 87.5, 90]
    prob = [0, 28, 31, 33, 35, 37, 39, 42, 44, 46, 49, 52, 53, 56, 57, 58, 59, 59, 58, 57, 55, 52, 48, 44, 39,
                      34, 30, 25, 21, 17, 15, 12, 10, 8, 0, 0, 0]
    prob_forest = np.zeros(len(slope_angle))
    prob_no_forest = np.zeros(len(slope_angle))
    #forest_effect =  np.zeros(len(slope_angle))

    for i in range(len(slope_angle)):
        prob_no_forest[i] = prob[i]*.77 # this .77 is taken from the paper from Stefan.
        # forest_reduction_function.append(1-(slope_angle[i]*.5))
        prob_forest[i] = prob[i]*.23
    forest_effect = (prob_no_forest-prob_forest)

    ############ plot this function##################
    fig, ax = plt.subplots()
    ax.set_xlabel('slope angle [deg]')
    ax.set_ylabel('relative likelihood [%]')
    ax.set_title('Soil slope failure (from Elmar, Steger & Glade 2017)')
   # ax.plot(slope_angle, prob, label='relative probability value', color='black')
    ax.plot(slope_angle, prob_no_forest, label='Release prob no forest', color='blue')
    ax.plot(slope_angle, prob_forest, label='Release prob forest', color='green')
    #ax.plot(slope_angle, forest_reduction_function, label='Prob reduction function', color='black')
    ax.plot(slope_angle, forest_effect, label = "Forest effect", color = "red")
    fig.legend(bbox_to_anchor=(0.9, 0.83))
    ax.grid(True, axis = "both",which ="major")
    ax.grid(True, axis="both", which="minor", linestyle= ":")
    plt.minorticks_on()
    #fig.tight_layout()
    #plt.show()


def avalanche():

    # for i in elh:
    #     alpha_plus[i] = max(0, elh[i]* slope + alpha_plus_max)
    #     velocity[i] = np.sqrt(elh[i] * 2 * g)

    g = 9.8 # m/s^2 gravity
    alpha_plus_max = 10 # deg No avalanches release in forest on slope of 35 deg bebi 2009
    elh = range(250) # max ekh = 250 m this is the grid for x axis

    """ 
     impact of 100 kPa will break well developed forest (Rapin 202) https://www.researchgate.net/profile/Francois_Rapin/publication/267562272_A_new_scale_for_avalanche_intensity/links/59ed9a88a6fdccef8b0dd7a4/A-new-scale-for-avalanche-intensity.pdf
     max speed of avalanche has been measured by radar 23m/s (Gubler 1987, http://hydrologie.org/redbooks/a162/iahs_162_0405.pdf)
     Therefore at about 200 kg/m^3 density going full speed @ 23m/s we have no more forest. 
     """
    no_forest_effect_velocity = 30 # m/s speed where forest no longer slow an avalanche.
    no_forest_effect_pt = (no_forest_effect_velocity^2)/(2*g) # [m] of energyline hight
    print("no forest effect velocity ", no_forest_effect_velocity," no forest effect energy line  ", no_forest_effect_pt)
    slope = (alpha_plus_max-0)/(0-no_forest_effect_pt)# rise/ run = y0-y1/x0-x1

    alpha_plus = np.zeros(len(elh))
    velocity = np.zeros(len(elh))



    for i in elh:
        alpha_plus[i] = max( 0, -elh[i]/4.5 + alpha_plus_max)
        velocity[i] = np.sqrt(elh[i]*2*g)

    impact = [20000, 50000, 100000, 170000]  # Pa
    avalanche_density = 200  # kg/m3
    velocity_points = []
    elh_points = []
    for i in range(len(impact)):
        velocity_points.append(math.sqrt(impact[i] / avalanche_density))
        elh_points.append(math.sqrt(impact[i] / avalanche_density)** 2 / (2 * 9.8))

    fig, ax = plt.subplots()

    color=iter(plt.cm.rainbow(np.linspace(0,1,len(elh_points)+2))) # +2 is just to get nicer colors from rainbow


    ax.plot(elh, alpha_plus, label=r'$\alpha_{increase \: forest \: (avalanche)}$ with respect to $Z_\delta$', color='black',linewidth=4)

    ax.set_xlabel(r'$Z_\delta$ [m]', size = 20)

    ax.set_ylabel(r'$\alpha_{increase \: forest \: (avalanche)}$ [deg]', size = 20)
    #ax.set_title('Avalanche')
    #ax.xaxis.grid(which='both')
    ax.yaxis.grid(which='both')
    #ax.grid(True)
    ax2 = ax.twiny()
    ax3 = ax2.twinx()
    ax3.set_ylabel("Effective runout angle " + r"$\alpha_{forest_i}$", size = 20)
    ax3.set_ylim(24.5, 35.5)
    ax.set_ylim(-.5, 10.5)
    ax2.plot(velocity ,alpha_plus, label = r'$\alpha_{increase \: forest \: (avalanche)}$ with respect to velocity', color = 'red',linewidth=4)
    for value in range(len(elh_points)):
        c = next(color)
        ax.scatter(elh_points[value], -elh_points[value]/4.5 + alpha_plus_max, label= str(impact[value]/1000) + " kPa assuming snow density of 200 [kg/m^3] ", color= c, s =100 )
        ax2.scatter(velocity_points[value],-elh_points[value]/4.5 + alpha_plus_max, color= c, s=100)

    ax2.set_xlabel('Avalanche velocity [m/s]', color = 'red', size =20 )
    ax2.axvline(x=23, label="max avalanche speed (Gubler, 1987)",  color = 'red', linestyle="--", linewidth=2)
    ax2.axvline(x=40, label="max avalanche speed (Fellin, 2013)",  color = 'red', linestyle=":",linewidth=2 ) # fellin 2013
    ax2.axvline(x=70, label="max avalanche speed  (Johannesson et al., 2009) ",  color = 'red', linestyle="-.", linewidth=2 ) # the design of avalanche protection dams
    fig.legend(loc="upper right",bbox_to_anchor=(.9, 0.85))
    ax2.grid(True)
    ax.tick_params(axis='both', which='major', labelsize=15)
    ax.tick_params(axis='both', which='both', labelsize=15)
    ax2.tick_params(axis='x', which='major', labelsize=15)
    ax3.tick_params(axis='y', which='both', labelsize=15, pad = 1)
    #fig.tight_layout()
    #plt.show()


def rockfall():
    g = 9.8  # m/s^2 gravity
    alpha_plus_max = 13  # Oswald 2019 - energy line height is increased by 6 deg in forest
    """ 
    significant influence on the rockfall process is only possible for rockfall velocity of approx.20 m / s  Rickli (2004)
    Therefore we put a no_forest_effect_pt at 30m/s so there is an effect at 20 [m/s]
     30 ^2 / (2*g) = 46 [m] elh ---> elh = v^2/(2g)
    """
    no_forest_effect_pt = 46 # [m] of energyline hight which is from a velocity of (30 [m/s])
    slope = (alpha_plus_max-0)/(0-no_forest_effect_pt)# rise/ run = y0-y1/x0-x1
    elh = range(127)  # max elh = 250 m so plot ends there

    alpha_plus = np.zeros(len(elh))
    velocity = np.zeros(len(elh))

    for i in elh:
        alpha_plus[i] = max(0, elh[i]* slope + alpha_plus_max)
        velocity[i] = np.sqrt(elh[i] * 2 * g)

    fig, ax = plt.subplots()

    ax.plot(elh, alpha_plus, label=r'$\alpha_{increase \: forest \: (rockfall)}$ with respect to $Z_\delta$', color='black')

    ax.set_xlabel(r'$Z_\delta$ [m]', size=20)
    ax.set_ylabel(r'$\alpha_{increase \: forest \: (rockfall)}$ [deg]', size=20)


    ax2 = ax.twiny()
    ax2.plot(velocity, alpha_plus, label= r'$\alpha_{increase \: forest \: (avalanche)}$ with respect to velocity', color='red')

    ax2.set_xlabel('Rockfall velocity [m/s]', color = 'red', size =20 )
    ax.axvline(x=20, label="significant influence of forest (Rickli, 2004)", color='purple', linestyle="--")
    ax2.axvline(x=30, label="max rockfall speed (Lateltin et al., 1997)", color='red', linestyle="--")
    ax2.axvline(x=50, label="average max speed (Dorren and Seijmonsbergen, 2003)", color='red', linestyle=":")  # fellin 2013
    #ax2.axvline(x=20, label="max avalanche speed  johannesson & others ", color='green', linestyle="-.")  # the design of avalanche protection dams
    ax2.plot((0,20),(7,7), color ="pink", linewidth=4, label="velocity range of forest influence *")
    ax2.plot((15,25),(6.8,6.8), color ="orange", linewidth=4, label="fockfall velocitys **") # Jahn, 1988 & others") , Zinggeler (1990), Gsteiger (1993), Doche (1997), Dorren et al. (2004b) and Perret et al. (2004)")
    ax2.xaxis.grid()

    ax3 = ax2.twinx()
    ax3.set_ylabel("Effective runout angle " + r"$\alpha_{forest_i}$", size = 20)

    ax3.set_ylim(31.5, 45.5)
    ax.set_ylim(-.5, 13.5)

    ax.tick_params(axis='both', which='major', labelsize=15)
    ax.tick_params(axis='both', which='both', labelsize=15)
    ax2.tick_params(axis='x', which='major', labelsize=15)
    ax3.tick_params(axis='y', which='both', labelsize=15, pad = 1)

    fig.legend(loc="upper right", bbox_to_anchor=(.9, 0.85))








def main():
    avalanche()
    soil()
    rockfall()
    plt.show()

if __name__ == "__main__":
    main()