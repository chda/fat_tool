
import numpy as np




def getLongestSeq(a):
    """
    This function will pick out the length of the longest energy line and the start index of the line
    This is useful so we only consider one energy line for our forest indicator.
    Problems of many energy lines come from not smooth terrain that may make many short (few meter) avalanches along the path.
    """
    n = len(a)
    maxIdx = 0
    maxLen = 0
    currLen = 0
    currIdx = 0

    for k in range(n):
        if a[k] > 0:
            currLen += 1
            # New sequence, store
            # beginning index.
            if currLen == 1:
                currIdx = k
        else:
            if currLen >= maxLen:
                maxLen = currLen
                maxIdx = currIdx
            currLen = 0
    if maxLen < currLen:
        maxLen = currLen
    elh = np.zeros_like(a)  # first initialize the elh with zeros

    if maxLen > 0:
        for cell_no in range(maxIdx, maxIdx + maxLen, 1):
            elh[cell_no] = a[cell_no] + elh[cell_no]
    else:
        print("No rockfall detected, slope not steep enough.")
    end = maxIdx + maxLen
    return elh, end
