"""
The core of this model will include the parts of the model that are not unique to a process.
1. Call in data         Responsible: Michael & Christopher
3. Call process type    Responsible: Avalanche-Christopher & Michael, Rockfall- Jean-baptiste
4. Plot results 1D      Responsible: Christopher
"""

import sys
import read_data as rd
from Avalanche import Avalanche
from Rockfall import rockfall
import tegrav
import pandas as pd
import damages
from Soil import Soil
import ResultsDisplay
import numpy as np
import json
from tegrav import create_new_df_row

def calculate(
    method : str,
    profile : np.ndarray,
    forest : np.ndarray,
    infra_roads : np.ndarray,
    infra_buildings : np.ndarray,
    options : dict
    ) -> ( np.ndarray, np.ndarray, np.ndarray, np.ndarray, pd.DataFrame, dict):

    """
    
    Parameters
    ----------
    method : str
        what method of calculation to use (avalanche, rock_fall, soil_slide)

    profile : np.ndarray
        prepared profile data in np array
    
    forest : np.ndarray
        prepared forest data np array

    infra_roads : np.ndarray
        prepared linear infrastructure data np array

    infra_buildings : np.ndarray
        prepared building infrastructure data np array

    options : dict
        prepared options data in a dictionary

    Returns
    -------
    np.ndarray
        profile
    
    np.ndarray
        forest indicator

    np.ndarray
        elh

    np.ndarray
        elh_forest
    
    pd.DataFrame
        table data

    dict
        dynamic text

    """
    dynamic_text =  []
    # user input
    avalanche = method is 'avalanche'
    rock_fall = method is 'rock_fall'
    soil_slide = method is 'soil_slide'

    channelize = options['channelize'] # this is only needed if soil slide is true.
    slope_width = options['slope_width']  # m
    rehab_cost_ha = options['rehab_cost_ha'] # euro/ha/y
    country = options['country'] # 'italy' 'france''austira''german''slovenia''unknown'
    
    afforest_range = options['afforest_range']
    afforest_start = options['afforest_start']
    afforest_type = options['afforest_type'] # 1 bush 2 mixed 3 evergreen (for avalanches)

    rehab_start = options['rehab_start']
    rehab_range = options['rehab_range']
    forest_type_rehab = options['forest_type_rehab']

    max_snow_height = options['max_snow_height']
    release_area = options['release_area']
    protection_location = options['protection_location'] # infra type ("building", "linear") this should be the line number of the infra file.

    # tegrave switches
    afforestation_runout = options['afforestation_runout']
    afforestation_release = options['afforestation_release']
    forest_rehab = options['forest_rehab']
    snow_bridges = options['snow_bridges']
    artificial_release = options['artificial_release']
    snow_stopping_dam = options['snow_stopping_dam']
    road_closure = options['road_closure']
    building_evacuation = options['building_evacuation']
    building_relocation = options['building_relocation']
    early_warning_system = options['early_waringing_system']
    rocknet = options['rocknet']


    # # Process Calculation
    if soil_slide is True:
        elh, forest_effect, hazard_end = Soil(profile,forest) # elh and elh_forest are the same for soil
        """
        No forest indicator or forest effect on runout for soil slides 
        we only adjust the prob of release for soil slides as a forest effect
        therefore, we hack in next two lines to return an empty array so outputs match avalanche and rockfall formats 
        """
        forest_indicator = np.zeros_like(elh)
        elh_forest = elh
    elif rock_fall is True:
        elh, elh_forest, forest_indicator, hazard_end, hazard_end_forest = rockfall(profile, forest)
    elif avalanche is True:
        elh, elh_forest, forest_indicator, hazard_end, hazard_end_forest = Avalanche(profile, forest)

    if elh[-1,1] > 0: # error for too short of a path for the runout
        print("Please load in a longer profile the hazards runout may be longer")

    """
    Tegrave core (calls damages.py and tegrav.py)
    This is the core for the economic model. 
    Francesca Poratelli and Cristian Assastello are responsible for the content 
    Christopher D'Amboise helped with the python and integration with the process model. 
    """
    # economic constants used by tegrav
    afforest_cost_ha = {'italy': 2930, 'france':4000, 'austira':3780, 'german':4000, 'slovenia':3885, 'unknown':4000}  # cost of afforestation in € per ha --> france and germany have unknown values
    people_value = 4000000
    interest_rate = 0.02
    tree_line_height= 2100

    # set up data frame
    df = pd.DataFrame(columns=['measure', 'direct_cost', 'indirect_cost', 'avoided_damages', 'benefits'])
    total_damages = damages.total_damages(infra_roads, infra_buildings, hazard_end, slope_width, probability_someone_hit=1)  # calculate initial damages with out forest

    for i in range(0, len(elh[:,0]), 1):
        index = i
        if elh[i,0] > 0:
            break
    start_height = profile[index,1] # hazard start elevation
    start_x = profile[index,0] # hazard start x location
    """ 
    location to build snow dam or rockfall net 
    this location will just before the infra that needs to be protected 
    """
    if snow_stopping_dam is True or rocknet is True:
        if protection_location[0] == "linear":
            location = int(infra_roads[protection_location[1], 1])
        elif protection_location[0] == "building":
            location = int(infra_buildings[protection_location[1], 1])

    ################################ Mitigation measures ######################

    # if afforestation_release is True:
    #     measure = "afforestation release area"
    #     years = [0, 25, 50, 100]
    #     if start_height < tree_line_height:
    #         #print("afforest start height is ", profile[start_height,1])
    #         print("afforest start height is ", profile[index, 1])
    #         direct_cost, indirect_cost = tegrav.ra_afforestation(release_area, afforest_cost_ha[country])
    #         for i in range(0, len(years), 1):
    #             reduced_damages = total_damages * 0.2
    #             avoided_damages = total_damages - reduced_damages
    #             new_row = {'Measure': measure + str(years[i]), 'Direct Cost': direct_cost[i], 'Indirect Cost': indirect_cost[i],
    #                        'Avoided Damages': avoided_damages, "Benefits": avoided_damages - indirect_cost[i]}
    #             df = df.append(new_row, ignore_index=True)
    #     else:
    #        print("Error: The afforested area that is picked is above treeline. Trees will never grow up there.")

    if afforestation_runout is True:
        afforest_ditc = { 'i18nKey': 'afforestation' }
        measure = "afforestation"
        years = [0, 25, 50, 100]
        if profile[afforest_start,1] < tree_line_height:
            if rock_fall is True or avalanche is True:
                direct_cost, indirect_cost, hazard_end_afforest, implementation_cost, maintenance = tegrav.afforestation(afforest_range, afforest_start, slope_width, forest, profile, afforest_cost_ha[country], afforest_type, avalanche, rock_fall )
            elif soil_slide is True:
                direct_cost, indirect_cost, hazard_end_afforest, implementation_cost, maintenance = tegrav.afforestation_soil(
                    afforest_range, afforest_start, slope_width, forest, profile, afforest_cost_ha[country],
                    afforest_type,channelize)

            reduced_damages = []
            avoided_damages = []
            for i in range(0, len(direct_cost), 1):
                reduced_damages.append(damages.total_damages(infra_roads, infra_buildings, hazard_end_afforest[i], slope_width, probability_someone_hit=1))
                avoided_damages.append(total_damages - reduced_damages[i])
                # new_row = {'measure': measure + str(years[i]), 'direct_cost': direct_cost[i], 'indirect_cost': indirect_cost[i],
                #         'avoided_damages': avoided_damages, "benefits": avoided_damages - indirect_cost[i]}

                new_row = create_new_df_row(measure + str(years[i]),  direct_cost[i], indirect_cost[i], avoided_damages[i])
                df = df.append(new_row, ignore_index=True)
        else:
            print("Error: The afforested area that is picked is above treeline. Trees will never grow up there.")

        afforest_ditc.update({'afforest_unit_cost': afforest_cost_ha[country],
                              'country': country,
                              'slope_width': slope_width,
                              'afforest_length': afforest_range,
                              'implementation_cost': implementation_cost,
                              'lifetime_maintenance':maintenance,
                              'direct_cost0': direct_cost[0],
                              'indirect_cost0': indirect_cost[0],
                              'avoided_damages0': avoided_damages[0],
                              'benefits0': avoided_damages[0] - indirect_cost[0],
                              'reduced_damages0': reduced_damages[0],
                              'direct_cost25': direct_cost[1],
                              'indirect_cost25': indirect_cost[1],
                              'avoided_damages25': avoided_damages[1],
                              'benefits25': avoided_damages[1] - indirect_cost[1],
                              'reduced_damages25': reduced_damages[1],
                              'direct_cost50': direct_cost[2],
                              'indirect_cost50': indirect_cost[2],
                              'avoided_damages50': avoided_damages[2],
                              'benefits50': avoided_damages[2] - indirect_cost[2],
                              'reduced_damages50': reduced_damages[2],
                              'direct_cost100': direct_cost[3],
                              'indirect_cost100': indirect_cost[3],
                              'avoided_damages100': avoided_damages[3],
                              'benefits100': avoided_damages[3] - indirect_cost[3],
                              'reduced_damages100': reduced_damages[3],
                              'total_damages': total_damages})
        dynamic_text.append(afforest_ditc)



    if (forest_rehab is True) and not soil_slide:
        rehab_ditc = {'i18nKey': 'forestRehab'}
        measure = "forestRehab"
        years = [0, 10, 20, 30]
        reduced_damages = []
        avoided_damages = []
        if profile[afforest_start,1] < tree_line_height:
            direct_cost, indirect_cost, hazard_end_afforest = tegrav.forest_rehab(rehab_start, rehab_range, slope_width, forest, profile, rehab_cost_ha, forest_type_rehab, avalanche, rock_fall)
            for i in range(0, len(direct_cost), 1):
                reduced_damages.append(damages.total_damages(infra_roads, infra_buildings, hazard_end_afforest[i], slope_width, probability_someone_hit=1))
                avoided_damages.append(total_damages - reduced_damages[i])
                # new_row = {'Measure': measure + str(years[i]), 'Direct Cost': direct_cost[i], 'Indirect Cost': indirect_cost[i],
                #         'Avoided Damages': avoided_damages, "Benefits": avoided_damages[i] - indirect_cost[i]}

                new_row = create_new_df_row(measure + str(years[i]),  direct_cost[i], indirect_cost[i], avoided_damages[i])

                df = df.append(new_row, ignore_index=True)
        else:
            print("Error: The forest area that is picked is above the treeline")

        rehab_ditc.update({'rehab_length': rehab_range,
                              'slope_width': slope_width,
                              'rehab_area': rehab_range * slope_width,
                              'forest_rehab_cost': rehab_cost_ha,
                              'forest_management_cost10': rehab_cost_ha * 10/10000,
                              'forest_management_cost20': rehab_cost_ha * 20 / 10000,
                              'forest_management_cost30': rehab_cost_ha * 30 / 10000,
                              'reduced_damages10': reduced_damages[0],
                              'avoided_damages10': total_damages - reduced_damages[0],
                              'indirect_cost10': indirect_cost[0],
                              'benefits10': avoided_damages[0] - indirect_cost[0],
                              'reduced_damages20': reduced_damages[1],
                              'avoided_damages20': total_damages - reduced_damages[1],
                              'indirect_cost20': indirect_cost[1],
                              'benefits20': avoided_damages[1] - indirect_cost[1],
                              'reduced_damages30': reduced_damages[2],
                              'avoided_damages30': total_damages - reduced_damages[2],
                              'indirect_cost30': indirect_cost[2],
                              'benefits30': avoided_damages[2] - indirect_cost[2],
                              'total_damages': total_damages})
        dynamic_text.append(rehab_ditc)

    if snow_bridges is True:
        bridges_dict = {}
        new_row, wording = tegrav.snow_bridges(max_snow_height, release_area, interest_rate, total_damages)
        df = df.append(new_row, ignore_index=True)
        bridges_dict.update(wording)
        bridges_dict.update({'avoided_damages': new_row["avoided_damages"],
                             'indirect_cost': new_row["indirect_cost"],
                             'benefits': new_row["benefits"],
                             'total_damages': total_damages})
        dynamic_text.append(bridges_dict)

    if (artificial_release is True) and avalanche:
        new_row, wording = tegrav.artificial_release(interest_rate, total_damages)
        df = df.append(new_row, ignore_index=True)
        # bomb_dict = {'i18nKey': 'artificialRelease'}
        bomb_dict = {}
        bomb_dict.update(wording)
        bomb_dict.update({'avoided_damages': new_row["avoided_damages"],
                            'indirect_cost': new_row["indirect_cost"],
                            'benefits': new_row["benefits"],
                            'total_damages': total_damages})
        dynamic_text.append(bomb_dict)

    if snow_stopping_dam is True:

        # snow stopping dam with out forest
        measure = "deviationDamNoForest"
        elh_infra = elh[location, 0] # energy line height at infra being protected.
        new_row, wording = tegrav.dam(location, elh_infra, slope_width, total_damages, measure, infra_buildings, infra_roads,hazard_end)
        df = df.append(new_row, ignore_index=True)

        # snow stopping dam with forest
        measure = "deviationDamForest"
        elh_infra = elh_forest[location, 0]
        new_row, wording= tegrav.dam(location, elh_infra, slope_width, total_damages, measure, infra_buildings, infra_roads,hazard_end)
        df = df.append(new_row, ignore_index=True)

        dam_dict = {'i18nKey': 'deviationDam'}
        dam_dict.update(wording)
        dam_dict.update({'avoided_damages': new_row["avoided_damages"],
                         'indirect_cost': new_row["indirect_cost"],
                         'benefits': new_row["benefits"],
                         'total_damages': total_damages})

        dynamic_text.append(dam_dict)

    if road_closure is True:
        new_row, wording = tegrav.road_closure(infra_roads, slope_width, hazard_end, total_damages, infra_roads, infra_buildings, people_value)
        df = df.append(new_row, ignore_index=True)
        # road_closure_dict = {'i18nKey': 'roadClosure'}
        road_closure_dict = {}
        road_closure_dict.update(wording)
        road_closure_dict.update({'avoided_damages': new_row["avoided_damages"],
                                  'indirect_cost': new_row["indirect_cost"],
                                  'benefits': new_row["benefits"],
                                  'total_damages': total_damages})
        dynamic_text.append(road_closure_dict)

    if building_evacuation is True:
        new_row, wording = tegrav.building_evacuation(infra_buildings,infra_roads, total_damages,infra_buildings,hazard_end, people_value, slope_width)
        df = df.append(new_row, ignore_index=True)
        # evacuation_dict = {'i18nKey': 'buildingEvac'}
        evacuation_dict = {}
        evacuation_dict.update(wording)
        evacuation_dict.update({'avoided_damages': new_row["avoided_damages"],
                               'indirect_cost': new_row["indirect_cost"],
                               'benefits': new_row["benefits"],
                               'total_damages': total_damages})
        dynamic_text.append(evacuation_dict)

    if building_relocation is True:
        new_row, wording = tegrav.building_relocation(infra_buildings, infra_roads, total_damages, people_value, hazard_end, slope_width)
        df = df.append(new_row, ignore_index=True)
        # relocation_dict = {'i18nKey': 'buildingReloc'}
        relocation_dict = {}
        relocation_dict.update(wording)
        relocation_dict.update({'avoided_damages': new_row["avoided_damages"],
                                'indirect_cost': new_row["indirect_cost"],
                                'benefits': new_row["benefits"],
                                'total_damages': total_damages})
        dynamic_text.append(relocation_dict)

    if early_warning_system is True:
        new_row, wording = tegrav.warning_system(infra_roads,total_damages, infra_roads, infra_buildings, hazard_end, slope_width, people_value)
        df = df.append(new_row, ignore_index=True)
        warning_sys_dict = {'i18nKey': 'earlyWarnSys'}
        warning_sys_dict.update(wording)
        warning_sys_dict.update({'avoided_damages': new_row["avoided_damages"],
                                'indirect_cost': new_row["indirect_cost"],
                                'benefits': new_row["benefits"],
                                'total_damages': total_damages})
        dynamic_text.append(warning_sys_dict)

    if (rocknet is True) and rock_fall:
        measure = "rocknetForest"
        elh_infra = elh_forest[location, 0]
        new_row, wording = tegrav.rocknet(location, elh_infra, slope_width, interest_rate, total_damages, measure,infra_buildings,infra_roads,hazard_end)

        df = df.append(new_row, ignore_index=True)

        measure = "rocknetNoForest"
        elh_infra = elh[location, 0]
        new_row, wording = tegrav.rocknet(location, elh_infra, slope_width, interest_rate, total_damages, measure,infra_buildings,infra_roads,hazard_end)

        df = df.append(new_row, ignore_index=True)
        nets_dict = {'i18nKey': 'rocknet'}
        nets_dict.update(wording)
        nets_dict.update({'avoided_damages': new_row["avoided_damages"],
                          'indirect_cost': new_row["indirect_cost"],
                          'benefits': new_row["benefits"],
                          'total_damages': total_damages})
        dynamic_text.append(nets_dict)
    ########################################################################
    #


    # Plotting Results
    #ResultsDisplay.turin_results(profile, forest, elh, elh_forest, forest_indicator)

    # print(df)
    #df.to_csv("tegrav_results.csv", index=False, encoding='utf8')

    print(dynamic_text)

    # with open(str('testets.json'), 'w') as f:
    #     json.dump(dynamic_text, f)


    return profile, forest_indicator, elh, elh_forest, df, dynamic_text
