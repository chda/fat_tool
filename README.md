# README #


### What is this repository for? ###
This repository is for development for the first models in a model chain that will quantify the value of a natural hazard protection forest. The model chain is called the protection Forest Assessment Tool (FAT). The first model in the chain are gravitational process models, rock fall, Avalanche and Debris slide. 

The inputs for the process model is:
- 2D profile consisting of sets of (X,Z) values.
- Forest location and characteristics 
For an example of the inputs see StPaul1.txt for profile and StPaulForest.txt for forest information. 

Process start areas are at start of 2D profile. 

The out put for these models are three vectors: 
- energy line height for profile not considering forest
- energy line height for profile considering forest
- forest indicator 

The forest indicator is a vector with the same length of the profile. The forest indicator is a value between 0 - 1 which is an indicator for forest effect along the path. Forest indicator value of 1 means there is a very strong forest effect, where indicator value of 0 means there is no effect. The forest indicator will be used to determine the economic value and is the main input for models down the chain. 

### File descriptions ###
core.py	- Runs model and picks hazard process
read_data.py	- loads data and resamples profile to 1m resolution
Avalanche.py	- Avalanche routine
Rockfall.py	- rock fall routine
ResultsDisplay.py - plotting routine 


### How do I get set up? ###
To run the example:
1. activate FAT python environment "source activate FAT"
2. run the .sh file which calls the input files "./run.sh"
The model is written in Python 3. All the necessary libraries are contained in the Anaconda environment provided, FAT.yml . 

Example profiles and forest information are included in the "example" folder. Plots from the provided example are also included in the "Example" folder.

### Contribution guidelines ###

To ensure smooth work-flow to all working on this project make sure to commit often and leave a descriptive commit message. 
This project is written in python 3, we are using the anaconda environment FAT.yml which is include in the repo. If a new library is needed make sure to export the updated anaconda environment.

### Who do I talk to? ###

Avalanche Christopher	BFW
Avalanche Michael  	BFW
Rock fall Jean-Baptiste IRSTEA

### TODO ###
TODO: make a start point not at start of profile. 
