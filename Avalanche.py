#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This is where the avalanche stopping criteria lives.
Responsible - Christopher & Michael
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import energy_line

def forest_idx(x, forest):
    '''
    Reads out the Position of the Forest
    ToDo: Calculate ForestValue out of the given Parameters
    '''
    forest_value = np.zeros_like(x)
    for line in forest:
        idx1 = np.where(x == line[1])[0]
        idx2 = np.where(x == line[2])[0]
        forest_type = np.where(x == line[3])[0] # 1 = bushes, 2 = mixed forest, 3 = evergreen

        forest_density =  line[4]#np.where(x == line[4])[0]
        if forest_type == 1:
            forest_value[idx1[0]:idx2[0]] = .2
        elif forest_type ==2:
            forest_value[idx1[0]:idx2[0]] = .05 + forest_density * .75
        elif forest_type ==3:
            forest_value[idx1[0]:idx2[0]] = .25 + forest_density * .75
        else:
            print("ohhh dang there is no forest info")
        if forest_density <= .2:
            forest_value[idx1[0]:idx2[0]] = .2
    return forest_value

def energy_line_height(x, z, alpha, alpha_sigma, max_elh, no_forest_effect_height,forest_value,alpha_forest):
    #forest_value = forest_idx(x, forest)
    elh = np.zeros_like(z)
    elh_plus = np.zeros_like(z)
    elh_minus = np.zeros_like(z)
    resolution = x[1] - x[0]

    for i in range(0, len(x) - 1):
        scaled_added_friction = (alpha_forest * forest_value[i])
        slope = (scaled_added_friction - 0) / (0 - no_forest_effect_height)  # rise/ run = y0-y1/x0-x1
        alpha_calc = alpha + max(0, elh[i]* slope + scaled_added_friction)

        elh[i + 1] = min(max_elh, max(0, elh[i] + (z[i] - z[i + 1]) - resolution * np.tan(np.deg2rad(alpha_calc))))

        #alpha_calc = alpha + max(0, -elh_plus[i] * (max_added_friction / 45) + max_added_friction)
        alpha_calc = alpha + max(0, elh_plus[i] * slope + scaled_added_friction)
        elh_plus[i + 1] = min(max_elh, max(0, elh_plus[i] + (z[i] - z[i + 1]) - resolution * np.tan(
            np.deg2rad(alpha_calc + alpha_sigma))))

        #alpha_calc = alpha + max(0, -elh_minus[i] * (max_added_friction / 45) + max_added_friction)
        alpha_calc = alpha + max(0, elh_minus[i] * slope + scaled_added_friction)
        elh_minus[i + 1] = min(max_elh, max(0, elh_minus[i] + (z[i] - z[i + 1]) - resolution * np.tan(
            np.deg2rad(alpha_calc - alpha_sigma))))

    # below function picks out the longest energy line in the case there are more than one line drawn.
    elh_processed, process_end = energy_line.getLongestSeq(elh)
    elh_plus_processed, process_end_plus = energy_line.getLongestSeq(elh_plus)
    elh_minus_processed, process_end_minus = energy_line.getLongestSeq(elh_minus)

    return elh_processed, elh_plus_processed, elh_minus_processed, process_end





def Avalanche(profile, forest):
    # Initialisation of Variables    
    alpha = 25
    alpha_forest = 10  # Increase of alpha angle in the forest
    alpha_sigma = 1.5  # standard deviation on alpha used for both with and without forest
    max_elh = 250 # for avalanches max elh is 250 from flow-r and max avalanche speed 70 johannesson & others book: the design of avalanche protection dams
    no_forest_effect_height = 45  # [m]
    """ 
    impact of 100 kPa will break well developed forest (Rapin 202) https://www.researchgate.net/profile/Francois_Rapin/publication/267562272_A_new_scale_for_avalanche_intensity/links/59ed9a88a6fdccef8b0dd7a4/A-new-scale-for-avalanche-intensity.pdf
    max speed of avalanche has been measured by radar 23m/s (Gubler 1987, http://hydrologie.org/redbooks/a162/iahs_162_0405.pdf)
    Therefore at about 200 kg/m^3 density going full speed @ 23m/s we have no more forest. 
    """
    x = profile[:, 0]
    z = profile[:, 1]    

    forest_value = forest_idx(x, forest)
    elh, elh_plus, elh_minus, avalanche_end= energy_line_height(x, z, alpha, alpha_sigma, max_elh, no_forest_effect_height, forest_value, 0) # no forest due to 0
    elh_forest, elh_forest_plus, elh_forest_minus, avalanche_end_forest = energy_line_height(x, z, alpha, alpha_sigma, max_elh, no_forest_effect_height, forest_value, alpha_forest) # forest
    #### here we make the forest indicator
    forest_indicator = np.zeros(len(z))
    forest_indicator_minus = np.zeros(len(z)) # calculated with minus sigma_alpha (1.5 deg) is alphas standard deviation
    forest_indicator_plus = np.zeros(len(z))  # calculated with plus sigma_alpha
    # % diff is used for forest indicator
    for i in range(1, len(x)):
        if elh[i] > 0:
            forest_indicator[i] = (elh[i] - elh_forest[i])/(elh[i] ) * 100
        elif elh[i] <= 0:
            forest_indicator[i] = 100

        if elh_minus[i] > 0:
            forest_indicator_minus[i] = (elh_minus[i] - elh_forest_minus[i])/(elh_minus[i] ) * 100
        elif elh_minus[i] <= 0:
            forest_indicator_minus[i] = 100

        if elh_plus[i] > 0:
            forest_indicator_plus[i] = (elh_plus[i] - elh_forest_plus[i]) / (elh_plus[i] ) * 100
        elif elh_plus[i]<= 0:
            forest_indicator_plus[i] = 100

    ELH = np.column_stack((elh, elh_plus, elh_minus))
    ELH_FOREST = np.column_stack((elh_forest, elh_forest_plus, elh_forest_minus))
    FOREST_INDICATOR = np.column_stack((forest_indicator, forest_indicator_plus, forest_indicator_minus))

    return ELH, ELH_FOREST, FOREST_INDICATOR, avalanche_end, avalanche_end_forest
