import numpy as np
import forest_growth
import math

def top_height_vs_elevation(forest_type,average_forest_elevation):
    # 1 = bushes, 2 = mixed forest, 3 = evergreen
    forest_type_dict = {1:"bushes", 2:"leafy", 3: "evergreen"}
    forest_type_string = forest_type_dict[forest_type] # we need this as a string to get it from the dict
    if forest_type == 1:
    #    print("Bushes not a valid afforestation measure")
        height = 2 #[m] bushes arent tall 

    else:
        growth_map = {'leafy': np.array([[2.50000000e+01, 1.67409526e+01],
                            [7.50000000e+01, 1.67409526e+01],
                            [1.25000000e+02, 1.67409526e+01],
                            [1.75000000e+02, 1.83985616e+01],
                            [2.25000000e+02, 2.15601515e+01],
                            [2.75000000e+02, 2.52708995e+01],
                            [3.25000000e+02, 2.79429924e+01],
                            [3.75000000e+02, 2.90682538e+01],
                            [4.25000000e+02, 2.89741304e+01],
                            [4.75000000e+02, 2.80124489e+01],
                            [5.25000000e+02, 2.76411866e+01],
                            [5.75000000e+02, 2.70892486e+01],
                            [6.25000000e+02, 2.63305843e+01],
                            [6.75000000e+02, 2.52339934e+01],
                            [7.25000000e+02, 2.41451619e+01],
                            [7.75000000e+02, 2.33051031e+01],
                            [8.25000000e+02, 2.22800197e+01],
                            [8.75000000e+02, 2.13374472e+01],
                            [9.25000000e+02, 2.02116144e+01],
                            [9.75000000e+02, 1.90864167e+01],
                            [1.02500000e+03, 1.81602558e+01],
                            [1.07500000e+03, 1.72194750e+01],
                            [1.12500000e+03, 1.65447970e+01],
                            [1.17500000e+03, 1.57439442e+01],
                            [1.22500000e+03, 1.51254022e+01],
                            [1.27500000e+03, 1.45444565e+01],
                            [1.32500000e+03, 1.39814463e+01],
                            [1.37500000e+03, 1.34896701e+01],
                            [1.42500000e+03, 1.30120111e+01],
                            [1.47500000e+03, 1.26410678e+01],
                            [1.52500000e+03, 1.22745383e+01],
                            [1.57500000e+03, 1.18832083e+01],
                            [1.62500000e+03, 1.15487010e+01],
                            [1.67500000e+03, 1.12843331e+01],
                            [1.72500000e+03, 1.09747644e+01],
                            [1.77500000e+03, 1.06013825e+01],
                            [1.82500000e+03, 1.02726560e+01],
                            [1.87500000e+03, 9.85563484e+00],
                            [1.92500000e+03, 9.43887419e+00],
                            [1.97500000e+03, 8.96757485e+00],
                            [2.02500000e+03, 8.47168951e+00],
                            [2.07500000e+03, 7.99258018e+00],
                            [2.12500000e+03, 7.46044762e+00],
                            [2.17500000e+03, 6.97876306e+00],
                            [2.22500000e+03, 6.52063460e+00],
                            [2.27500000e+03, 6.03676604e+00],
                            [2.32500000e+03, 5.55837234e+00],
                            [2.37500000e+03, 5.07195723e+00],
                            [2.42500000e+03, 4.57960457e+00],
                            [2.47500000e+03, 4.11337148e+00],
                            [2.52500000e+03, 3.69168853e+00],
                            [2.57500000e+03, 3.28127921e+00],
                            [2.62500000e+03, 2.80713480e+00],
                            [2.67500000e+03, 2.38258031e+00],
                            [2.72500000e+03, 2.04721757e+00],
                            [2.77500000e+03, 1.54970437e+00],
                            [2.82500000e+03, 1.21365393e+00],
                            [2.87500000e+03, 9.64449823e-01],
                            [2.92500000e+03, 6.55284570e-01]]),
                            'evergreen': np.array([[25., 1.67409526e+01],
                            [75., 1.67409526e+01],
                            [125., 22.85012143],
                            [175., 24.53701804],
                            [225., 27.19170588],
                            [275., 30.2579586],
                            [325., 33.19839958],
                            [375., 34.31784225],
                            [425., 34.64340437],
                            [475., 34.9496345],
                            [525., 35.23134615],
                            [575., 35.25651502],
                            [625., 35.2483987],
                            [675., 35.01503446],
                            [725., 34.69675373],
                            [775., 34.45698687],
                            [825., 33.95609182],
                            [875., 33.59391895],
                            [925., 33.15677173],
                            [975., 32.620015],
                            [1025., 32.18174262],
                            [1075., 31.63922892],
                            [1125., 31.17355169],
                            [1175., 30.5185789],
                            [1225., 30.02434818],
                            [1275., 29.39518248],
                            [1325., 28.63598466],
                            [1375., 27.9484435],
                            [1425., 27.136375],
                            [1475., 26.49696202],
                            [1525., 25.64604487],
                            [1575., 24.84289794],
                            [1625., 24.01078627],
                            [1675., 23.32099614],
                            [1725., 22.62499891],
                            [1775., 21.73215951],
                            [1825., 21.04924254],
                            [1875., 20.27034663],
                            [1925., 19.6143339],
                            [1975., 18.78322146],
                            [2025., 18.12103857],
                            [2075., 17.39464198],
                            [2125., 16.65241559],
                            [2175., 15.87122616],
                            [2225., 15.19446568],
                            [2275., 14.57243415],
                            [2325., 13.97635068],
                            [2375., 13.30071663],
                            [2425., 12.66601817],
                            [2475., 12.04376057],
                            [2525., 11.50693434],
                            [2575., 10.91114707],
                            [2625., 10.33007201],
                            [2675., 9.77152554],
                            [2725., 9.2698175],
                            [2775., 8.67927488],
                            [2825., 8.23457152],
                            [2875., 7.87591393],
                            [2925., 7.16870737]])}

        elevation = np.asarray(growth_map[forest_type_string][:, 0]) # this saves the elevation as an array
        top_height = growth_map[forest_type_string][:, 1]
        index = (np.abs(elevation - average_forest_elevation)).argmin()  # finds the closet index to elevation
        height = top_height[index]

    return height



def forest_growth(c0, forest_type ):
    """
    forest_type  1 = bushes, 2 = mixed forest, 3 = evergreen

    UPDATE FROM GEORG KINDERMANN following Kindermann (2018) and update with Data from the Austrian National Forest Inventory (ANFI 1981-2019)
    """
    top_height = [0]
    for age in [25,50, 100]:
        if forest_type == 3:

            # c0 = #value taken from forest raster Austrian National Forest Inventory (ANFI 1981-2019)
            #Forest type conifer forest (spruce):
            hkc1 = 0.75
            hkc3 =  3
            hkcc0 = -11.14612447
            hkcc1 = 0.02023767
            hkcc2 = 1.90310459

            height = c0 * math.log(1 + math.exp(hkcc0 + hkcc1 * c0 ** hkcc2) * age ** hkc3) ** hkc1


        elif forest_type == 2: # leaf trees
            #Forest type broad-leaved forest (beech)


            hkc1 = 0.75
            hkc3 = 3
            hkcc0= -12
            hkcc1 = 5.071840
            hkcc2 = 27.419350
            hkcc3 = -2.830829


            height = c0 * math.log(1 + math.exp(hkcc0 + hkcc1 / (1 + math.exp(hkcc2 + hkcc3 * c0))) * age ** hkc3) ** hkc1

        else:
            height = 1.5 # bushes hieght
        top_height.append(height)
    return top_height

def FSI_rockfall(top_height, forest_type):
    """
    calculates FSI values for the 4 growth periods for afforestaion measures.
    :param top_height:
    :return: fsi a 4 element array for forest structure index for years 0, 25, 50 and 100
    """
    forest_type_potential = {1: .2 , 2: 1, 3: .8} # max forest potential bush = 1, leafy = 2, evergreen =3

    forest_potential = forest_type_potential[forest_type]  # this is the weight factor for different forests
    fsi = []
    for height in top_height:
        if height >= 0 and height < 10: #0 - 10 meter trees
            FSI = height * 9

        elif height >= 10 and height < 20:
            FSI = height + 80

        elif height >= 20 and height < 40:
            FSI = height * -5 + 200

        elif height > 40:
            FSI = 0

        else:
            FSI = 0 # should never be negative values of height but in case.

        fsi.append(max(.2, FSI/100*forest_potential))    # here we take the calculated FSI value and adjust it to forest type, min value is .2

    return fsi


def FSI_avalanche(top_height, forest_type):
    """
    calculates FSI values for the 4 growth periods for afforestaion measures.
    :param top_height:
    :return: fsi a 4 element array for forest structure index for years 0, 25, 50 and 100
    """
    forest_type_potential = {1: .2 , 2: .8, 3: 1} # max forest potential bush = 1, leafy = 2, evergreen =3

    forest_potential = forest_type_potential[forest_type]  # this is the weight factor for different forests
    fsi = []
    for height in top_height:
        if height >= 0 and height < 10: #0 - 10 meter trees
            FSI = height * 9

        elif height >= 10 and height < 20:
            FSI = 1 #height + 80

        elif height >= 20 and height < 40:
            FSI = height * -5 + 200

        elif height > 40:
            FSI = 0

        else:
            FSI = 0 # should never be negative values of height but in case.

        fsi.append(max(.2, FSI/100*forest_potential))    # here we take the calculated FSI value and adjust it to forest type, min value is .2

    return fsi


def forest_grow(forest_type, average_forest_elevation, avalanche, rockfall):

    c0 = top_height_vs_elevation(forest_type, average_forest_elevation)  # returns top height from 100 year trees

    # find top height for years 0, 25,50 and 100
    top_height_array = forest_growth(c0, forest_type)  # top height array is the top height for years 0, 25, 50 and 100

    if rockfall is True:
        fsi = FSI_rockfall(top_height_array, forest_type)
    elif avalanche is True:
        fsi = FSI_avalanche(top_height_array, forest_type)


    return fsi


if __name__ == "__main__":
    forest_type = 3
    average_forest_elevation = 2925
    process = 'rockfall'
    # find top height for year 100
    c0 = top_height_vs_elevation(forest_type, average_forest_elevation) # returns top height from 100 year trees

    # find top height for years 0, 25,50 and 100
    top_height_array = forest_growth(c0, forest_type) # top height array is the top height for years 0, 25, 50 and 100

    # scale top height to FSI value
    if process == "rockfall":
        fsi = FSI_rockfall(top_height_array,forest_type)

    print(fsi)
