#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 28 09:07:11 2019

@author: lawinenforschung
Script reading in the profile and forest from .txt
"""


import numpy as np
from scipy import interpolate


def regrid(resolution, profile):
    '''
    Interpolation of Points along the Profile with a certain resolution
    '''
    x = np.arange(0, profile[-1, 0], resolution)
    #inter = interpolate.interp1d(profile[: , 0], profile[: , 1])
    inter = interpolate.interp1d(profile[: , 0], profile[: , 1], axis=0, fill_value="extrapolate")
    z_new = inter(x)
    profile_resolution = np.column_stack((x, z_new))
    return profile_resolution

# Reading data:
def read_path(profile_filename):

    profile = np.genfromtxt(profile_filename, skip_header=1)  # [X, Z]
    # profile_res = regrid(1, profile)    this line has been moved to ProcessHandler constructor
    return profile

def read_forest(forest_filename):
    '''
    [ID, X1, X2, NrTree, DbhMean, DbhStd]
    When only one forest is stored in the data it will be read in as a 1D array.
    To have always the same structure we make a 2D array out of it.
    '''
    
    forest = np.genfromtxt(forest_filename, skip_header=1)
    if len(forest.shape) == 1:
        forest = np.array([forest])    
    return forest


def read_infra(infra_filename):
    '''
    [index, x of infra, type, value
    When only one infrastructure is stored in the data it will be read in as a 1D array.
    To have always the same structure we make a 2D array out of it.
    '''

    infra = np.genfromtxt(infra_filename, skip_header=3)
    if len(infra.shape) == 1:
        forest = np.array([infra])
    return infra

