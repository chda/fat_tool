import matplotlib.pyplot as plt
import numpy
from scipy.stats import binned_statistic
import rasterio
import sys


def read_header(input_file):
    # Reads in the header of the raster file, input: filepath

    raster = rasterio.open(input_file)
    if raster is None:
        print('Unable to open {}'.format(input_file))
        sys.exit(1)

    header = {}
    header['ncols'] = raster.width
    header['nrows'] = raster.height
    header['xllcorner'] = (raster.transform * (0, 0))[0]
    header['yllcorner'] = (raster.transform * (0, raster.height))[1]
    header['cellsize'] = raster.transform[0]
    header['noDataValue'] = raster.nodata
    return header


def read_raster(input_file):
    header = read_header(input_file)
    raster = rasterio.open(input_file)
    my_array = raster.read(1)

    return my_array, header


def output_raster(file, file_out, raster):
    """Input is the original file, path to new file, raster_data, and the EPSG Code"""

    raster_trans = rasterio.open(file)
    try:
        crs = rasterio.crs.CRS.from_dict(raster_trans.crs.data)
    except:
        crs = rasterio.crs.CRS.from_epsg(32632)
    if file_out[-3:] == 'asc':
        new_dataset = rasterio.open(file_out, 'w', driver='AAIGrid', height=raster.shape[0], width=raster.shape[1],
                                    count=1, dtype=raster.dtype, crs=crs, transform=raster_trans.transform,
                                    nodata=-9999)
    if file_out[-3:] == 'tif':
        new_dataset = rasterio.open(file_out, 'w', driver='GTiff', height=raster.shape[0], width=raster.shape[1],
                                    count=1, dtype=raster.dtype, crs=crs, transform=raster_trans.transform,
                                    nodata=-9999)
    new_dataset.write(raster, 1)
    new_dataset.close()


def output_raster_v2(file_out, raster, epsg):
    """Input is path to file, raster_data"""

    # raster_trans = rasterio.open(file)
    crs = rasterio.crs.CRS.from_epsg(epsg)
    new_dataset = rasterio.open(file_out, 'w', driver='GTiff', height=raster.shape[0], width=raster.shape[1], count=1,
                                dtype=raster.dtype, crs=crs, nodata=-9999)
    new_dataset.write(raster, 1)
    new_dataset.close()

def data_cleaner(dem, raster):
    """
    Calls in 2 raster files one DEM and one raster with data.
    makes a 2d array from the spatial data.
    data is cleaned by removeing all the 0 values and the no data value which should be a very large -neg number

    function returns results from binned_statistics from 2d array.
    in this case it is the average raster from each elevation band.
    """
    dem_1d = dem.flatten()
    raster_1d = raster.flatten()

    raster_clean = numpy.column_stack((dem_1d,raster_1d))
    raster_clean = raster_clean[raster_clean[:,1] > 0 ]


    bin = range(0,3000,50)
    bin_mean, bin_edges, bin_num = binned_statistic(raster_clean[:,0], raster_clean[:,1], 'mean', bins=bin)
    #bin_std, bin_edges, bin_num = binned_statistic(trees_clean[:, 0], trees_clean[:, 1], 'std', bins=50)

    return bin_mean, bin_edges, bin_num, raster_clean







if __name__ == "__main__":

    #input_dem = "/home/maverick/OneDrive/documents/GR4A/plots/austrian_forest_height_vs_elevation/seehoehe.tif"
    input_dem = "/home/maverick/Documents/GR4A/forest_growth_fat/seehoehe.tif"
    dem, dem_header = read_raster(input_dem)
    cols, rows = dem.shape # row 297, col 575

    #input_evergreen = "/home/maverick/OneDrive/documents/GR4A/plots/austrian_forest_height_vs_elevation/fichte.tif"
    input_evergreen = "/home/maverick/Documents/GR4A/forest_growth_fat/fichte.tif"
    evergreen, evergreen_header = read_raster(input_evergreen)


    #input_leafy = "/home/maverick/OneDrive/documents/GR4A/plots/austrian_forest_height_vs_elevation/RBuche.tif"
    input_leafy = "/home/maverick/Documents/GR4A/forest_growth_fat/RBuche.tif"
    leafy, leafy_header = read_raster(input_leafy)

    bin_mean, bin_edges, bin_num, evergreen_clean =data_cleaner(dem, evergreen) # evergreen clean is a 2d array of elevation vs tree height
    bin_center = numpy.zeros_like(bin_mean)
    #range_bin = len(bin_edges)
    for i in range(0,len(bin_center),1):
        bin_center[i] = (bin_edges[i]+bin_edges[i+1])/2 # find mid point of each bin so we can attach the average to it

    evergreen = numpy.column_stack((bin_center, bin_mean ))

    bin_mean, bin_edges, bin_num, leafy_clean = data_cleaner(dem, leafy)
    bin_center = numpy.zeros_like(bin_mean)
    #range_bin = len(bin_edges)
    for i in range(0,len(bin_center),1):
        bin_center[i] = (bin_edges[i]+bin_edges[i+1])/2
    leafy = numpy.column_stack((bin_center, bin_mean))

    print(leafy)
    print(evergreen)
    cuecumber  = {"leafy" : leafy, "evergreen":evergreen}
    print(cuecumber)


    fig1, ax1 = plt.subplots()
    ax1.set_xlabel('elevation above sea level [m]', fontsize=20)
    ax1.set_ylabel('Projected top height @ 100 years[m]', fontsize=20)
    ax1.set_title('Tree size @ 100 years vs elevation (evergreen)', fontsize=26)
    ax1.scatter(evergreen_clean[:,0], evergreen_clean[:,1], label='evergreen', color='black')
    ax1.hlines(evergreen[:,1], bin_edges[:-1], bin_edges[1:], colors='pink', lw=5, label='binned statistic of data')
    fig1.legend(bbox_to_anchor=(0.9, 0.83))
    ax1.grid(True, axis="both", which="major")
    ax1.grid(True, axis="both", which="minor", linestyle=":")
    ax1.set_ylim(0, 50)
    plt.minorticks_on()

    fig2, ax2 = plt.subplots()
    ax2.set_xlabel('elevation above sea level [m]', fontsize=20)
    ax2.set_ylabel('Projected top height @ 100 years[m]', fontsize=20)
    ax2.set_title('Tree top height @ 100 years vs elevation (Beech)', fontsize=26)
    ax2.scatter(leafy_clean[:,0], leafy_clean[:,1], label='evergreen', color='black')
    ax2.hlines(leafy[:,1], bin_edges[:-1], bin_edges[1:], colors='pink', lw=5, label='binned statistic of data')
    fig2.legend(bbox_to_anchor=(0.9, 0.83))
    ax2.grid(True, axis="both", which="major")
    ax2.grid(True, axis="both", which="minor", linestyle=":")
    ax2.set_ylim(0, 50)
    plt.minorticks_on()


    plt.show()