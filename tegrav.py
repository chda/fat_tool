#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 17 14:42:49 2019

@author: DISAF
"""
from Avalanche import Avalanche
from Rockfall import rockfall
import matplotlib.pyplot as plt
import numpy as np
import damages
from lookup import forest_grow
import ResultsDisplay as RD
from Soil import Soil as soil

def create_new_df_row(measure, direct_cost, indirect_cost, avoided_damages):

    return {'measure': measure, 'direct_cost': direct_cost, 'indirect_cost': indirect_cost,
               'avoided_damages': avoided_damages, "benefits": avoided_damages - indirect_cost}

# # AVALANCHE RELEASE AREA
# def ra_afforestation(release_area,afforest_cost_ha): #afforestation + wooden tripods in the release area
#     afforest_cost = afforest_cost_ha / 10000 # euro/m^2
#     tripod_cost =  245 * 600 / 10000 # 245euro/unit * 600/ ha * ha/10000 m^2
#     construction_cost = (afforest_cost + tripod_cost) * release_area
#     construction_cost_plant = afforest_cost * release_area
#     maintenance = .1 * construction_cost_plant
#     direct_cost= construction_cost + maintenance
#     indirect_cost= 0
#     return direct_cost, indirect_cost

def snow_bridges(max_snow_height, release_area, interest_rate, total_damages):
    measure = "snowBridges"
    if max_snow_height <= 2: #m
        unitary_cost = 400 #€/m
    elif max_snow_height >2 and max_snow_height <= 4: #m
        unitary_cost = 600 #€/m
    elif max_snow_height > 4: #m
        unitary_cost = 800 #€/m
    lifetime = 70 #years
    maintenance_quote = 0.05
    dismantling_quote = 0.8
    bridge_density = 400/ 10000 # [bridges per m2]  400 bridges per ha * 1ha/10000m^2
    #calculated values
    construction_cost = unitary_cost*release_area*bridge_density #the release area has to be provided by the user in m^2
    maintenance_cost = maintenance_quote*construction_cost
    dismantling_cost = dismantling_quote*construction_cost*1/(1+interest_rate)**lifetime
    direct_cost = construction_cost + maintenance_cost + dismantling_cost
    indirect_cost = 0

    reduced_damages = total_damages * 0.01 # this is because there is small prob that an event will happen but if it does then the damage is still the same
    avoided_damages = total_damages - reduced_damages
    # new_row = {'Measure': measure, 'Direct Cost': direct_cost, 'Indirect Cost': indirect_cost,
    #            'Avoided Damages': avoided_damages, "Benefits": avoided_damages - indirect_cost}

    new_row = create_new_df_row(measure, direct_cost, indirect_cost, avoided_damages)

    wording= {
                'i18nKey': measure,
                "unit_cost":unitary_cost,
                "release_area":release_area,
                "construction_cost":construction_cost,
                "dismantling_cost": dismantling_cost,
                "mantenance_cost":maintenance_cost,
                "reduced_damages": reduced_damages
             }

    return new_row, wording

def artificial_release (interest_rate, total_damages) :
    """
    do we need years in here?
    """
    measure = "artificialRelease"
    #fixed values
    construction_cost = 100000 #€/unit
    lifetime = 40
    maintenance = 950 #€/year
    #calculated values
    maintenance_cost = maintenance*((((1+interest_rate)**lifetime)-1)/(interest_rate*((1+interest_rate)**lifetime)))
    direct_cost = construction_cost + maintenance_cost
    indirect_cost=0
    reduced_damages = total_damages * 0.01  # this is because there is small prob that an event will happen but if it does then the damage is still the same
    avoided_damages = total_damages - reduced_damages
    # new_row = {'Measure': measure, 'Direct Cost': direct_cost, 'Indirect Cost': indirect_cost,
    #            'Avoided Damages': avoided_damages, "Benefits": avoided_damages - indirect_cost}

    new_row = create_new_df_row(measure, direct_cost, indirect_cost, avoided_damages)

    wording = {
                "i18nKey": measure,
                "construction_cost": construction_cost,
                "lifetime_mantenance": maintenance_cost,
                "reduced_damages": reduced_damages
               }

    return new_row, wording


def afforestation_soil( forest_range, start, slope_width, forest, profile, afforest_cost_ha, forest_type, channelized):

        afforest_cost = afforest_cost_ha / 10000  # euro/m^2
        tripod_cost = 245 * 600 / 10000  # 245euro/unit * 600 units/ ha * ha/10000 m^2
        implementation_cost = (afforest_cost + tripod_cost) * forest_range * slope_width
        construction_cost_plant = afforest_cost * forest_range * slope_width
        # adding "forest" to the forest file
        end = start + forest_range
        average_forest_elevation = (start + end) / 2
        forest_id = forest[-1, 0] + 1

        # forest_development = forest_grow(forest_type, average_forest_elevation, avalanche, rock_fall)
        forest_development = [0, 1, 1, 1]

        years = [0, 25, 50, 100]
        direct_cost = []
        new_hazard_end = []
        indirect_cost = []
        for i in range(0, len(forest_development), 1):
            print(i, years[i], forest_development[i])
            afforest = np.append(forest, [[forest_id, start, end, forest_type, forest_development[i]]], axis=0)

            elh, forest_effect, hazard_end = soil(profile, afforest)  # elh1 should be the same as elh both with out forest layers

            maintenance = .1 * construction_cost_plant * years[i] / 100  #
            direct_cost.append(implementation_cost + maintenance)
            new_hazard_end.append(hazard_end)
            indirect_cost.append(0)

        return direct_cost, indirect_cost, new_hazard_end, implementation_cost, maintenance


def afforestation(afforest_range, afforest_start, slope_width, forest, profile, afforest_cost_ha, forest_type, avalanche, rock_fall ):


    afforest_cost = afforest_cost_ha / 10000 # euro/m^2
    tripod_cost =  245 * 600 / 10000 # 245euro/unit * 600 units/ ha * ha/10000 m^2
    implementation_cost = (afforest_cost + tripod_cost) * afforest_range * slope_width
    construction_cost_plant = afforest_cost * afforest_range * slope_width
    # adding "forest" to the forest file
    end = afforest_start + afforest_range
    average_forest_elevation = (afforest_start + end)/2
    forest_id = forest[-1, 0] + 1
    forest_development = forest_grow(forest_type, average_forest_elevation, avalanche, rock_fall)

    ################### all to enhance FSI value to highighted areas ###########
    x = profile[:, 0]
    fsi = np.zeros_like(x)
    forest_type_array = np.zeros_like(x)
    afforest_array = np.zeros_like(x)

    for line in forest:
        start_section = int(line[1]) #must be int so we can use it for index
        forest_end = int(line[2])
        forest_length = forest_end - start_section
        for i in range(forest_length):
            print(i, i + afforest_start, len(fsi))
            fsi[start_section+i] = int(line[4] * 100) # make FSI into an int so we can sort it with the where loop
            forest_type_array[start_section + i] = line[3] #make an array with the type of forest.. not sure if this is needed yet
    no_forest_index = np.flatnonzero(forest_type_array ==0)
    for p in range(afforest_range):
        if afforest_start + p in no_forest_index:
            afforest_array[afforest_start + p] = 1

    forest_changes = np.where(np.diff(afforest_array, prepend=np.nan))[0]  # break forested areas
    n = 0  # index used in loop to get n+1 forest change
    afforest = []
    for i in forest_changes:
        if afforest_array[i] > 0:
            forest_length = x[forest_changes[n + 1]] - x[i]
            afforest.append([n, x[i], forest_length, forest_type_array[i],
                             fsi[i] / 100])  # fsi needs to be turned back into index 0-1
        n += 1
    afforest = np.array(afforest)


    years = [0, 25, 50, 100]
    direct_cost = []
    new_hazard_end = []
    indirect_cost = []
    for i in range(0, len(forest_development), 1):
        forest_new = forest
        print(i, years[i], forest_development[i])
        for line in afforest:
            forest_id = forest_new[-1, 0] + 1
            afforest_start = line[1]
            end = line[2]
            newrow= np.array([forest_id, afforest_start, end, forest_type, forest_development[i]])
            forest_new = np.vstack((forest, newrow))

        if avalanche is True:
            elh1, elh_afforest, afforest_indicatore, hazard_end, hazard_end_forest = Avalanche(profile, forest_new)  # elh1 should be the same as elh both with out forest layers
        elif rock_fall is True:
            elh1, elh_afforest, afforest_indicatore, hazard_end, hazard_end_forest = rockfall(profile, forest_new)  # elh1 should be the same as elh both with out forest layers

        maintenance = .1 * construction_cost_plant * years[i] / 100  #
        direct_cost.append(implementation_cost + maintenance)
        new_hazard_end.append(hazard_end_forest)
        indirect_cost.append(0)

    return direct_cost, indirect_cost, new_hazard_end, implementation_cost, maintenance


def forest_rehab (rehab_start, rehab_range, slope_width, forest, profile, rehab_cost_ha, forest_type, avalanche, rock_fall): #re run on the avalanche model -> user input on where to place the forest
    """
    this function needs lots of work. maybe it should look kinda like afforestation
    where we rerun avalanche model with a change in forest density over time.
    """
    forest_management_cost = slope_width * rehab_cost_ha/10000 # €/m^2/y

    years = [0, 10, 20, 30]
    direct_cost = []
    new_hazard_end = []
    indirect_cost = []
    x = profile[:, 0]
    fsi = np.zeros_like(x)
    forest_type_array = np.zeros_like(x)

    ################### all to enhance FSI value to highighted areas ###########
    for line in forest:
        start_section = int(line[1]) #must be int so we can use it for index
        forest_end = int(line[2])
        forest_length = forest_end - start_section
        for i in range(forest_length):

            fsi[start_section+i] = int(line[4] * 100) # make FSI into an int so we can sort it with the where loop
            forest_type_array[start_section + i] = line[3] #make an array with the type of forest.. not sure if this is needed yet

    for j in range(0, 3, 1): # loop over the 3 time periods
        for k in range(rehab_range):  # loop over afforested area
            if fsi[rehab_start + k] > 0:
                fsi[rehab_start + k] = max(1, fsi[rehab_start + k] + (j+1)*10)  # change affored area to be better by 10% of max

        forest_changes = np.where(np.diff(fsi, prepend=np.nan))[0]  # break forested areas

        n = 0  # index used in loop to get n+1 forest change
        afforest = []
        for i in forest_changes:
            if fsi[i] > 0:
                forest_length = x[forest_changes[n + 1]] - x[i]
                afforest.append([n, x[i], forest_length, forest_type_array[i],
                                 fsi[i] / 100])  # fsi needs to be turned back into index 0-1
            n += 1
        afforest = np.array(afforest)

        ##########################

        if avalanche is True:
            elh1, elh_afforest, afforest_indicatore, hazard_end, hazard_end_forest = Avalanche(profile, afforest)  # elh1 should be the same as elh both with out forest layers
        elif rock_fall is True:
            elh1, elh_afforest, afforest_indicatore, hazard_end, hazard_end_forest = rockfall(profile, afforest)  # elh1 should be the same as elh both with out forest layers
        else:
            print("error afforestation routine does not know what hazard to model")
            continue

        management = (forest_management_cost* years[j])  #
        direct_cost.append(management)
        new_hazard_end.append(hazard_end_forest)
        #RD.turin_results(profile, afforest, elh1, elh_afforest, afforest_indicatore)
        indirect_cost.append(0)
#    management_unit_cost = 500 #€/ha/year

    return direct_cost, indirect_cost,new_hazard_end

def dam(location_dam, elh_infra, slope_width,total_damages, measure, infra_buildings,infra_linear, avalanche_end):
    """
    Should we add "year" into this function for:
    direct_cost =  construction_cost + maintenance* year[0, 25, 50, 100]
    """
    # fixed values
    unitary_cost = 500 # €/m^3 cost of a snow dam euro per m3
    #calculated values
    construction_cost= unitary_cost * elh_infra * slope_width
    maintenance = .1* construction_cost #
    direct_cost =  construction_cost + maintenance
    indirect_cost = 0

    probability_someone_hit = 0.01 # after dam

    reduced_damages = damages.total_damages_dam(infra_linear, infra_buildings, avalanche_end,  slope_width, probability_someone_hit, location_dam)
    avoided_damages = total_damages - reduced_damages
    # new_row = {'Measure': measure, 'Direct Cost': direct_cost, 'Indirect Cost': indirect_cost,
    #            'Avoided Damages': avoided_damages, "Benefits": avoided_damages - indirect_cost}

    new_row = create_new_df_row(measure, direct_cost, indirect_cost, avoided_damages)

    wording = {"elh_infra": elh_infra,
               "slope_width": slope_width,
               "construction_cost": construction_cost,
               "mantenance_cost": maintenance,
               "reduced_damages": reduced_damages
               }

    return new_row, wording

# Avoidance

def road_closure (linear_infrastructure, slope_width, hazard_end, total_damages, infra_roads, infra_buildings, people_value):

    measure = "roadClosure"
    value = {1: 100, 2: 1200, 3: 2400, 4: 4000, 5: 3000, 6: 220}  # euro/m
    car_value = {1: 0, 2: 10000, 3: 10000, 4: 10000, 5: 500000, 6: 0}  # euros
    number_people = {1: 0, 2: 2, 3: 2, 4: 2, 5: 40, 6: 0}  # 5. trains 40 people is a train car at half capacity
    indirect_cost = 0
    wording = {"slope_width": slope_width, 'repeted_text' : list()}

    sum_infra_value = 0 #initalize the sum to export for wording
    sum_vehicle_value = 0
    sum_people_value = 0
    sum_indirect_cost=0

    km_cost = 3
 
    for line in linear_infrastructure:
        index = int(line[0])
        x_location = line[1]
        road_type = line[2]
        traffic_intensity = line[3]
        deroute = line[4]

        if x_location <= hazard_end:
            total_infra_value = value[road_type] * slope_width
            total_vehicle_value = car_value[road_type] * traffic_intensity
            total_people_value = number_people[road_type] * traffic_intensity * people_value

            if (road_type == 2) or (road_type == 3) or (road_type == 4): # primary roads, secondary roads and highways
                indirect_cost = traffic_intensity * deroute + value[road_type] * slope_width

            else: # forest roads, trains and power lines
                indirect_cost = 0

            indirect_cost = deroute * traffic_intensity * km_cost
            sum_indirect_cost += indirect_cost
            total_infra_value = value[road_type] * slope_width
            sum_infra_value += total_infra_value
            total_vehicle_value = car_value[road_type] * traffic_intensity
            sum_vehicle_value += total_vehicle_value
            total_people_value = number_people[road_type] * traffic_intensity * people_value
            sum_people_value += total_people_value
            # wording.update({"traffic_intensity{}".format(index): traffic_intensity,
            #                 "road_value{}".format(index): value[road_type],
            #                 "car_value{}".format(index): car_value[road_type],
            #                 "deroute{}".format(index): deroute,
            #                 "number_people{}".format(index): number_people[road_type],
            #                 "total_infra_value{}".format(index): total_infra_value,
            #                 "total_traffcic_value{}".format(index): total_vehicle_value,
            #                 "total_people_value{}".format(index): total_people_value,
            #                 "indirect_cost{}".format(index): indirect_cost
            #                 })

            wording['repeted_text'].append( 
                            {
                                "index" : index + 1,
                                "traffic_intensity" : traffic_intensity,
                                "road_value": value[road_type],
                                "car_value": car_value[road_type],
                                "deroute": deroute,
                                "number_people": number_people[road_type],
                                "total_infra_value": total_infra_value,
                                "total_traffcic_value": total_vehicle_value,
                                "total_people_value": total_people_value,
                                "indirect_cost": indirect_cost
                            })


            """
            wording.update({"deroute{}".format(index): deroute,
                    "traffic_intensity{}".format(index): traffic_intensity,
                    "road_value{}".format(index): value[road_type],
                    "total_infra_value{}".format(index): total_infra_value,
                    "total_traffcic_value{}".format(index): total_vehicle_value,
                    "total_people_value{}".format(index): total_people_value,
                    "car_value{}".format(index): car_value[road_type],
                    "number_people{}".format(index): number_people[road_type]})
            """

    direct_cost=0
    probability_someone_hit = 0
    recalculated_road_damages = damages.road_damage(infra_roads, slope_width, hazard_end, people_value, probability_someone_hit) #linear_infra, slope_width, release_probability, hazard_end, people_value
    probability_someone_hit = 1
    recalculated_building_damages = damages.building_damages(infra_buildings, hazard_end, people_value, probability_someone_hit)
    reduced_damages = recalculated_road_damages + recalculated_building_damages
    avoided_damages = total_damages - reduced_damages
    # new_row = {'Measure': measure, 'Direct Cost': direct_cost, 'Indirect Cost': indirect_cost,
    #            'Avoided Damages': avoided_damages, "Benefits": avoided_damages - indirect_cost}

    new_row = create_new_df_row(measure, direct_cost, indirect_cost, avoided_damages)

    wording.update({
                    'i18nKey': measure,
                    "reduced_damages": reduced_damages,
                    "sum_infra_value": sum_infra_value,
                    "sum_vehicle_value": sum_vehicle_value,
                    "sum_people_value": sum_people_value
                    })


    return new_row, wording

def building_evacuation(infra_building, infra_roads, total_damages,infra_buildings,hazard_end, people_value, slope_width):
    measure = "buildingEvac"
    accomidation_value = 40 # euros/night
    indirect_cost = 0
    value = {1: 1200, 2: 1900, 3: 2600, 4: 2000}  # line[3]

    direct_cost = 0

    probability_someone_hit = 1
    recalculated_road_damages = damages.road_damage(infra_roads, slope_width, hazard_end, people_value, probability_someone_hit)  # linear_infra, slope_width, release_probability, hazard_end, people_value
    probability_someone_hit = 0
    recalculated_building_damages = damages.building_damages(infra_building, hazard_end, people_value, probability_someone_hit)
    reduced_damages = recalculated_road_damages + recalculated_building_damages

    avoided_damages = total_damages - reduced_damages
    # new_row = {'Measure': measure, 'Direct Cost': direct_cost, 'Indirect Cost': indirect_cost,
    #            'Avoided Damages': avoided_damages, "Benefits": avoided_damages - indirect_cost}

    new_row = create_new_df_row(measure, direct_cost, indirect_cost, avoided_damages)

    wording = {"reduced_damages": reduced_damages, "i18nKey" : measure, 'repeted_text' : list()}

    for line in infra_building:
        index = line[0]
        num_person = line[4]
        building_type = line[2]
        building_dimension = line[3]

        indirect_cost = accomidation_value * num_person
        total_people_value = people_value * num_person
        total_building_value = building_dimension * value[building_type]
        total_damages_building = total_people_value + total_building_value

        #wording.update({
        wording['repeted_text'].append({
            "index": index + 1,
            "building_type": building_type,
            "people_num": num_person,
            "indirect_cost":indirect_cost,
            "total_people_value" : total_people_value,
            "building_dimension": building_dimension,
            "building_value": value[building_type],
            "total_building_value" : total_building_value,
            "total_damages_building" : total_damages_building,
            "reduced_damages": reduced_damages
        })

    return new_row, wording

def building_relocation(infra_building, linear_infra ,total_damages, people_value, hazard_end, slope_width):
    """
    update name of function
    do we need the if elif loops?
    """
    measure = "buildingReloc"

    dismantling_quote = 0.8
    compensation_quote = 0.2
    direct_cost = 0
    indirect_cost = 0
    value = {1: 1200, 2: 1900, 3: 2600, 4: 2000}  # line[3] dimention
    for line in infra_building:  # loop over every line in infra_building.txt

        building_type = line[2]
        building_dimension = line[3]
        num_person = line[4]
        building_cost = value[building_type] * building_dimension
        dismantling_cost = dismantling_quote * building_cost
        indirect_cost += compensation_quote * building_cost
        direct_cost += building_cost + dismantling_cost

    probability_someone_hit = 1
    reduced_damages = damages.road_damage(linear_infra, slope_width, hazard_end, people_value, probability_someone_hit) # The reduced damages, in this case, are made up of everything that doesn't get relocated (for example the roads/railways), while the buildings and therefore the people inside won't get damaged.
    avoided_damages = total_damages - reduced_damages

    new_row = create_new_df_row(measure, direct_cost, indirect_cost, avoided_damages)

    wording = {"i18nKey" : measure, 'repeted_text': list()}

    for line in infra_building:
        index = line[0]
        num_person = line[4]
        building_type = line[2]
        building_dimension = line[3]

        #indirect_cost = accomidation_value * num_person
        total_people_value = people_value * num_person
        total_building_value = building_dimension * value[building_type]
        dismantling_cost = .8 * total_building_value
        compensation_cost = .2 * total_building_value
        direct_cost_building = total_building_value * dismantling_cost

        wording['repeted_text'].append(
            {
            "index" : index + 1,
            "building_type": building_type,
            "people_num": num_person,
            "indirect_cost": compensation_cost,
            "total_people_value": total_people_value,
            "building_dimension": building_dimension,
            "building_value": value[building_type],
            "total_building_value": total_building_value,
            "dismantling_cost" : dismantling_cost,
            "compensation_cost": compensation_cost,
            "direct_cost": direct_cost_building,
            "total_damages": total_damages,
            "reduced_damages": reduced_damages
        })

    return new_row, wording

def warning_system(infra_road, total_damages, infra_roads, infra_buildings, hazard_end, slope_width, people_value):
    """
    years?
    """
    measure = "earlyWarnSys"
    value = {1: 100, 2: 1200, 3: 2400, 4: 4000, 5: 3000, 6: 220}  # euro/m
    car_value = {1: 0, 2: 10000, 3: 10000, 4: 10000, 5: 500000, 6: 0}  # euros
    number_people = {1: 0, 2: 2, 3: 2, 4: 2, 5: 40, 6: 0}  # 5. trains 40 people is a train car at half capacity

    km_cost = 3 # euro/km
    unitary_cost = 500  # €
    maintenance = 50 # €/y
    lifetime = 20  # years
    # calculated values
    maintenance_cost =maintenance*lifetime
    direct_cost = unitary_cost + maintenance_cost
    wording = {
                "i18nKey" : measure,
                "maintenance_cost": maintenance_cost,
                "slope_width": slope_width,
                "repeted_text": list()}

    sum_infra_value = 0 #initalize the sum to export for wording
    sum_vehicle_value = 0
    sum_people_value = 0
    sum_indirect_cost=0

    for line in infra_road:
        index = line[0]
        road_type = line[2]
        traffic_intensity = line[3]
        deroute = line[4]
        indirect_cost = deroute*traffic_intensity*km_cost
        sum_indirect_cost += indirect_cost
        total_infra_value = value[road_type]*slope_width
        sum_infra_value += total_infra_value
        total_vehicle_value = car_value[road_type]* traffic_intensity
        sum_vehicle_value += total_vehicle_value
        total_people_value = number_people[road_type]*traffic_intensity* people_value
        sum_people_value += total_people_value
        wording['repeted_text'].append({
                        "index" : index + 1,
                        "traffic_intensity": traffic_intensity,
                        "road_value": value[road_type],
                        "car_value": car_value[road_type],
                        "deroute": deroute,
                        "number_people": number_people[road_type],
                        "total_infra_value": total_infra_value,
                        "total_traffcic_value": total_vehicle_value,
                        "total_people_value": total_people_value,
                        "indirect_cost" : indirect_cost
                        })

    reduced_damages = damages.total_damages(infra_roads, infra_buildings, hazard_end, slope_width,
                                            probability_someone_hit=0)
    avoided_damages = total_damages - reduced_damages
    # new_row = {'Measure': measure, 'Direct Cost': direct_cost, 'Indirect Cost': sum_indirect_cost,
    #            'Avoided Damages': avoided_damages, "Benefits": avoided_damages - indirect_cost}

    new_row = create_new_df_row(measure, direct_cost, indirect_cost, avoided_damages)

    wording.update({"reduced_damages": reduced_damages,
                    "sum_infra_value": sum_infra_value,
                    "sum_vehicle_value": sum_vehicle_value,
                    "sum_people_value": sum_people_value
                    })

    return new_row, wording

#
def rocknet(net_location, elh_infra, slope_width, int_rate, total_damages , measure, infra_buildings,infra_roads,hazard_end):
    # fixed values
    unitary_cost = 250  # cost of a rockfall net in euro per m2
    life_time = 25  # [years]
    probability_someone_hit = 1

    # calculated values
    construction_cost = unitary_cost * elh_infra * slope_width
    maintenance = .1 * construction_cost
    q = 1 + int_rate
    dismantling = (.5 * construction_cost) / (q ** life_time)
    direct_cost = construction_cost + maintenance + dismantling
    indirect_cost = 0


    reduced_damages = damages.total_damages_dam(infra_roads, infra_buildings, hazard_end, slope_width,
                                                probability_someone_hit, net_location)

    avoided_damages = total_damages - reduced_damages
    # new_row = {'Measure': measure, 'Direct Cost': direct_cost, 'Indirect Cost': indirect_cost,
    #            'Avoided Damages': avoided_damages, "Benefits": avoided_damages - indirect_cost}

    new_row = create_new_df_row(measure, direct_cost, indirect_cost, avoided_damages)

    wording = {"elh_infra": elh_infra,
               "slope_width": slope_width,
               "construction_cost": construction_cost,
               "mantenance_cost": maintenance,
               "dismantling_cost": dismantling,
               "reduced_damages": reduced_damages
               }

    return new_row, wording