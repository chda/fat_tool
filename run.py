"""
from Services.ProcessHandler import ProcessHandler
from pathlib import Path

input_path = Path('example/').resolve()
output_file_path = Path.cwd() / 'output.json'
profile = 'arzleralm.txt'
forest = 'arzleralm_forest.txt'
options = 'options.json'
infra_roads = 'infra.txt'
infra_buildings = 'infra_building.txt'


process_handler = ProcessHandler.from_files(
    input_path,
    profile,
    forest,
    infra_roads,
    infra_buildings,
    options
    )

#process_handler.avalanche(output_file_path)
process_handler.rock_fall(output_file_path)
#process_handler.soil_slide(output_file_path)
"""
from Services.ProcessHandler import ProcessHandler
from pathlib import Path

input_path = Path('example/AO').resolve() / 'landslide_debug.json'
output_file_path = Path('example/AO').resolve() / 'output_apptest.json'
process_handler = ProcessHandler.from_json(input_path)
process_handler.soil_slide(output_file_path)