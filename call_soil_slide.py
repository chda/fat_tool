import sys
from Services.ProcessHandler import ProcessHandler
from pathlib import Path

# """
# "path/to/python" "path/to/call_soil_slide.py" "path/to/input/folder" "path/to/output/folder" "profile.txt" "forest.txt" "infra_roads.txt" "infra_buildings.txt" "options.json" "output.json"
# """

# input_args = sys.argv
# process_handler = ProcessHandler.from_args(input_args)

# output = Path(input_args[2]) / input_args[8]

# process_handler.soil_slide(output)


""" cli command to run from anywhere (json version)
"path/to/python" "path/to/call_soil_slide.py" "path/to/input_file.json" "path/to/output_file.json"
"""

input_file = Path(sys.argv[1])
output_file = Path(sys.argv[2])

process_handler = ProcessHandler.from_json(input_file)

process_handler.soil_slide(output_file)