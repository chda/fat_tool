#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This is where the avalanche stopping criteria lives.
Responsible - Christopher & Michael
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy

def soil_lookup(start_slope):
    slope_angle =   [ 0, 2.5,  5, 7.5, 10, 12.5, 15, 17.5, 20, 22.5, 25, 27.5, 30, 32.5, 35, 37.5, 40, 42.5, 45, 47.5, 50, 52.5, 55, 57.5, 60, 62.5, 65, 67.5, 70, 72.5, 75, 77.5, 80, 82.5, 85, 87.5, 90]
    prob_no_forest =[ 0,  28, 31,  33, 35,   37, 39,   42, 44,   46, 49,   52, 53,   56, 57,   58, 59,   59, 58,   57, 55,   52, 48,   44, 39,   34, 30,   25, 21,   17, 15,   12, 10,    8,  0,    0, 0]
    prob_forest = []
    forest_reduction_function = []
    slope_reduction = []
    for i in range(len(slope_angle)):
        slope_reduction.append(i)
        forest_reduction_function.append(max(0,(-.5*slope_angle[i]+ 40)))
        #forest_reduction_function.append(1-(slope_angle[i]*.5))
        prob_forest.append(max(0,(1-forest_reduction_function[i]/100)* prob_no_forest[i]))
        #print(slope_angle[i]," prob reduction ", slope_angle[i]*.5, "inverse ",1 - (slope_angle[i]*.5)/100, "prob no forest ", prob_no_forest[i], ' prob forest ',prob_forest[i])


    event_prob = scipy.interpolate.interp1d(slope_angle, prob_no_forest)
    event_prob_forest = scipy.interpolate.interp1d(slope_angle, prob_forest)
    interpolate_no_forest = event_prob(start_slope)
    interpolate_forest = event_prob_forest(start_slope)
   #  ############ plot this function##################
   #  fig, ax = plt.subplots()
   #
   #  ax.plot(slope_angle, prob_no_forest, label='no forest', color='blue')
   #  ax.plot(slope_angle, prob_forest, label=' Forest', color='green')
   #  ax.plot(slope_angle, forest_reduction_function, label=' Forest reduction function', color='black')
   #  fig.legend(loc='right', bbox_to_anchor=(0.8, 0.83), ncol=2)
   #  ax.grid(True, axis = "both",which ="major")
   #  ax.grid(True, axis="both", which="minor", linestyle= ":")
   #  plt.minorticks_on()
   #  fig.tight_layout()
   # # plt.show()
    return interpolate_no_forest, interpolate_forest

def getLongestSeq(a):
    """
    This function will pick out the length of the longest energy line and the start index of the line
    This is useful so we only consider one energy line for our forest indicator.
    Problems of many energy lines come from not smooth terrain that may make many short (few meter) avalanches along the path.
    """
    n = len(a)
    maxIdx = 0
    maxLen = 0
    currLen = 0
    currIdx = 0

    for k in range(n):
        if a[k] > 0:
            currLen +=1
            # New sequence, store
            # beginning index.
            if currLen == 1:
                currIdx = k
        else:
            if currLen > maxLen:
                maxLen = currLen
                maxIdx = currIdx
            currLen = 0
        if maxLen < currLen:
            maxLen = currLen
    elh = np.zeros_like(a) # first initialize the elh with zeros

    if maxLen > 0:
        for cell_no in range (maxIdx, maxIdx+maxLen,1):
            elh[cell_no] = a[cell_no] + elh[cell_no]
    else:
        print("No landslide detected, slope not steep enough.")
    landslide_end = maxIdx+maxLen
    landslide_start = maxIdx
    return elh, landslide_end, landslide_start


def forest_idx(x, forest):
    '''
    Reads out the Position of the Forest
    ToDo: Calculate ForestValue out of the given Parameters
    '''
    forest_value = np.zeros_like(x)
    for line in forest:
        idx1 = np.where(x == line[1])[0]
        idx2 = np.where(x == line[2])[0]
        forest_value[idx1[0]:idx2[0]] = 1
    return forest_value


def Soil(profile, forest):

    # Initialisation of Variables
    #if channelize is True:
     #   alpha = 22
    #else:
    alpha = 28
    x = profile[:, 0]
    z = profile[:, 1]
    resolution = x[1] - x[0]

    # Starting with the Calculation
    forest_value = forest_idx(x, forest)
    elh = np.zeros_like(z) #[z[0]]

    for i in range(0,len(x)-1):
        # Calculation of EnergyLineHight without Forest
        elh[i+1] = min(250, max(0, elh[i] + (z[i] - z[i + 1]) - resolution * np.tan(np.deg2rad(alpha))))


    elh = np.asarray(elh) #  elh to an array not list
    longest, end, start = getLongestSeq(elh)


    # calculate slope at start point
    start_slope = abs((z[start] - z[start + 1] )/ (x[start]- x[start +1]))
    event_prob, event_prob_forest = soil_lookup(start_slope)
    if forest_value[start]>0: # if soil slide start is in forest..
        forest_effect = event_prob-event_prob_forest
    else:
        forest_effect = 0
        event_prob_forest = event_prob

    print("forest effect ", forest_effect, " event prob  no forest ",event_prob, " event prob froest ",event_prob_forest)
    nofelh = np.column_stack((elh, elh, elh))
    # fig, ax = plt.subplots()
    #
    # ax.plot(x, z, label='profile', color='black')
    # ax.plot(x, elh+z, label='energy line heigh', color='green')
    # #ax.plot(slope_angle, forest_reduction_function, label=' Forest reduction function', color='black')
    #
    # ax.plot(x, longest + z, label='longest slide', color='blue')
    # fig.legend(loc='right', bbox_to_anchor=(0.8, 0.83), ncol=2)
    # ax.grid(True, axis = "both",which ="major")
    # ax.grid(True, axis="both", which="minor", linestyle= ":")
    # plt.minorticks_on()
    # fig.tight_layout()
    # plt.show()


    return nofelh, forest_effect, end