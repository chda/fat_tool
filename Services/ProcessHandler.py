import read_data as rd
import pandas as pd
import json
from pathlib import Path
import numpy as np
from core import calculate
from typing import Union

class ProcessHandler:

    """Class that deals with calling FAT-TOOL methods"""

    def __init__(self, profile : np.ndarray, forest : np.ndarray, infra_roads: np.ndarray, infra_buildings : np.ndarray, options : dict) :

        """
        Creates an instance of ProcessHandler with required data

        Parameters
        ----------
        profile : np.ndarray
            profile data (is regrided here)
        
        forest : np.ndarray
            forest data in array form

        infra_roads : np.ndarray
            linear infra data in array form

        infra_buildings : np.ndarray
           buildings infra data in array form

        options : dict
            options in dictionary format, they are automatically merged with default options 

        """

        # regrid and merging default options is done here now so it doesn't have to be on 2 places
        profile = rd.regrid(1, profile)
        options = ProcessHandler._merge_options(options)

        self.profile = profile
        self.forest = forest
        self.infra_roads = infra_roads
        self.infra_buildings =infra_buildings
        self.options = options


    @staticmethod
    def from_files(input_dir_path : Path, profile_name : str, forest_name : str, infra_roads_name : str, infra_buildings_name : str, options_name : str):

        """
        Factory method to produce an instance of ProcessHandler from specified files

        Parameters
        ----------
        input_dir_path : Path
            input folder containing all the input paths
        
        profile_name : str
            name of the file from which the profile is loaded

        forest_name : str
            name of the file from which the forest data is loaded

        infra_roads_name : str
            name of the file from which the roads infrastructure is loaded

        infra_buildings_name : str
            name of the file from which building infrastructure is loaded

        options_name : str
            name of the file from which options are loaded

        
        Returns
        -------
        ProcessHandler
            instance of ProcessHandler
        """

        profile = rd.read_path(ProcessHandler.get_input_path(input_dir_path, profile_name))
        
        forest = rd.read_forest(ProcessHandler.get_input_path(input_dir_path, forest_name))
        infra_roads = rd.read_infra(ProcessHandler.get_input_path(input_dir_path, infra_roads_name))
        infra_buildings = rd.read_infra(ProcessHandler.get_input_path(input_dir_path, infra_buildings_name))


        with open(str(ProcessHandler.get_input_path(input_dir_path, options_name))) as json_file:
            options = json.load(json_file)

        return ProcessHandler(
            profile,
            forest,
            infra_roads,
            infra_buildings,
            options
        )

    @staticmethod
    def from_json(input_file_path : Path):

        """
        Factory method to produce an instance of ProcessHandler from json input file

        Parameters
        ----------
        input_file_path : Path
            Path to json file with all the data

        Returns
        -------
        ProcessHandler
            instance of ProcessHandler
        """

        with open(str(input_file_path)) as json_file:
            data = json.load(json_file)


        profile = np.column_stack((data['slope']['x'], data['slope']['y']))
        
        # in case order cannot be guarenteed, hardcode key names
        forest = ProcessHandler._load_json_data(data['forest'], ['id', 'x1', 'x2', 'type', 'density'])
        building = ProcessHandler._load_json_data(data['infraBuilding'], ['index', 'x', 'type', 'dimension', 'people'])
        roads = ProcessHandler._load_json_data(data['infraLinear'], ['index', 'x', 'type', 'traffic', 'deroute'])
        
        options = data['options']

        return ProcessHandler(
            profile,
            forest,
            roads,
            building,
            options
        )


    @staticmethod
    def from_args(args : list):

        """
        Factory method to produce an instance of ProcessHandler from an ordered input arguments list

        Parameters
        ----------
        args : list
            ordered list of required input arguments:
            [input_dir_path, output_dir_path, profile_file_name, forest_file_name, infra_roads_file_name, infra_buildings_file_name, options_file_name]

        Returns
        -------
        ProcessHandler
            instance of ProcessHandler
        """

        input_path = Path(args[1])
        profile = args[3]
        forest = args[4]
        roads = args[5]
        building = args[6]
        options = args[7]

        return ProcessHandler.from_files(
            input_path,
            profile,
            forest,
            roads,
            building,
            options
        )

    def avalanche(self, output_file_path : Path):

        """
        calls avalanche procedure and saves the results to an output file in file specified with output_file_path

        Parameters
        ----------
        output_file_path: Path
            path to output file

        """
        self._calculate('avalanche', output_file_path)

    def rock_fall(self, output_file_path : Path):

        """
        calls rock_fall procedure and saves the results to an output file in file specified with output_file_path

        Parameters
        ----------
        output_file_path: Path
            path to output file

        """
        self._calculate('rock_fall', output_file_path)

    def soil_slide(self, output_file_path : Path):

        """
        calls soil_slide procedure and saves the results to an output file in file specified with output_file_path

        Parameters
        ----------
        output_file_path: Path
            path to output file

        """

        self._calculate('soil_slide', output_file_path)


    def _calculate(self, method :str, output_file_path : Path):
        
        """
        does calculations based on specified method and input and saves the results in file specified with output_file_path

        Parameters
        ----------

        method : str
            name of method to use for calculation (avalanche, rock_fall, soil_slide)

        output_file_path: Path
            path to output file

        """

        profile, forest_indicator, elh, elh_forest, df, dt = calculate(
            method,
            self.profile,
            self.forest,
            self.infra_roads,
            self.infra_buildings,
            self.options
        )

        self._save_output_data(method, output_file_path, profile, forest_indicator, elh, elh_forest, df, dt)

    @staticmethod
    def get_input_path(input_dir: Path, file_name : str) -> Path:

        """
        returns full path to an input file

        Parameters
        ----------
        file_name : str
            the name of the file

        Returns
        -------
        Path
            full path to file
        """

        return input_dir / file_name


    @staticmethod 
    def _merge_options(options : dict) -> dict:

        """
        merges options with default values from default_options.json

        Parameters
        ----------
        options : dict
            options to use when running the calculations

        Returns
        -------
        dict
            a dictionary with prepared options
        """

        default_options_path = (Path(__file__).parent / 'default_options.json').resolve()

        with open(str(default_options_path)) as json_file:
            default_data = json.load(json_file)

        return {**default_data, **options}



    def _save_output_data(
            self, method : str,
            output_file_path: Path,
            profile : np.ndarray,
            forest_indicator: Union[np.ndarray, float],
            elh : np.ndarray,
            elh_forest: np.ndarray,
            df : pd.DataFrame,
            dt : dict
            ):

        """
        saves output data to a json file in the output folder

        Parameters
        ----------
        output_file_path: Path
            path to filename of the output file

        profile : np.ndarray
            profile data. Needed for x values.

        forest_indicator : np.ndarray
            forest indicator values with: avg, high, low

        elh : np.ndarray,
            elh with avg, high, low

        elh_forest: np.ndarray,
            elh forest with avg, high, low

        df: pd.DataFrame
           tegrav results

        dt: dict
           dynamic text 

        """          

        json_output = {}
        json_output['table_data'] = json.loads(df.to_json(orient="records"))
        json_output['dynamic_text'] = dt
       
        # if method == 'soil_slide':
        #      json_output['forest_indicator'] = forest_indicator

        # else:
        json_output['forest_indicator'] = {
            'x' : profile[:,0].tolist(),
            'low' : forest_indicator[:,1].tolist(),
            'avg' : forest_indicator[:,0].tolist(),
            'high' : forest_indicator[:,2].tolist(),

        }

        json_output['elh'] = {
            'x' : profile[:,0].tolist(),
            'low' : elh[:,1].tolist(),
            'avg' : elh[:,0].tolist(),
            'high' : elh[:,2].tolist(),
        }

        json_output['elh_forest'] = {
            'x' : profile[:,0].tolist(),
            'low' : elh_forest[:,1].tolist(),
            'avg' : elh_forest[:,0].tolist(),
            'high' : elh_forest[:,2].tolist(),
        }

        json_output['profile'] = {
            'x' : profile[:,0].tolist(),
            'y' : profile[:,1].tolist(),
        }

        with open(str(output_file_path), 'w') as f:
            json.dump(json_output, f)



    @staticmethod
    def _load_json_data(json_data : dict, keys : list) -> np.ndarray:

        """
        reads a json formatted dictionary with lists as values 
        and reforms it to np array resembling table data
        using keys list for which values to use and in what order

    
        Parameters
        ----------
        json_data : dict
            dictionary with keys and list data to reformat to np array

        keys : list
            keys, which to use to generate a np array from. They also specify the order.


        Returns
        -------
        data : np.ndarray
            np array created from json_data using keys


        Example 
        -------   
        json =  {
                    'a' : [1, 2, 3],
                    'b' : [4, 5, 6],
                    'c' : [7, 8, 9]
                }

        data = _load_json_data(json, ['b', 'c', 'a']) 
                
                [[4, 7, 1],
                 [5, 8, 2],
                 [6, 9, 3]]
        """
    
        width = len(keys) 
        height = len(json_data[keys[0]])

        data = np.zeros((height, width))

        for y in range(height):

            x = 0

            for key in keys:
                data[y][x] = json_data[key][y]
                x += 1

        return data
    