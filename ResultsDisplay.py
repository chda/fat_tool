#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 29 10:57:13 2019

@author: lawinenforschung
Plotting the results from the different processes
Input from read_data: Profile, Forest, maybe Infrastructure?
Input from Calculation: NoForestELH, ForestELH, Forest_Indicator
"""

import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import numpy as np

def plot_results(profile, forest, nofelh, felh, fi):
    fig, ax = plt.subplots()
  
    ax.plot(profile[:, 0], profile[:, 1], label='Profile', color='black')
    ax.plot(nofelh[:, 0], nofelh[:, 1], label='ELH No Forest')
    ax.plot(felh[:, 0], felh[:, 1], label='ELH Forest', color='green')
            
    for line in forest:
        ax.axvspan(line[1], line[2], facecolor='#2ca02c', alpha=0.4)
    
    ax1 = ax.twinx()
    ax1.plot(fi[:, 0], fi[:, 1], label='Forest Indicator', color='red')
#    ax.spines['top'].set_color('#dddddd') 
    ax1.spines['right'].set_color('red')
    ax1.tick_params(axis='y', colors='red')
    ax1.yaxis.set_major_formatter(mtick.PercentFormatter())
    #ax1.set_ylim([0, 100])
    ax.set_xlabel('Distance [m]')
    ax.set_ylabel('Altitude [m]')
    ax.set_title('FAT Results', y=1.2)   
    ax.grid()
    
    #ax1.set_yticks(np.linspace(0, ax1.get_ybound()[1]-5, 5))
    #ax.set_yticks(np.linspace(ax.get_ybound()[0]+5, ax.get_ybound()[1]-5, 4))
    
    fig.legend(loc='right', bbox_to_anchor=(0.8, 0.83), ncol=2)
    fig.tight_layout()
    fig.savefig('Energyline_withFI.png', dpi=300, bbox_inches="tight")
    plt.show()


def plot_avalanche(profile, forest, elh, elh_forest, forest_indicator):
    fig = plt.figure()
    ax2 = fig.add_axes([0.1, 0.5, 0.8, 0.4], xticklabels=[], )
    ax2.set_xlim([profile[0,0],profile[-1,0]])
    ax = fig.add_axes([0.1, 0.1, 0.8, 0.4],)
    ax.set_xlim([profile[0,0],profile[-1,0]])
    #profile
    ax.plot(profile[:, 0], profile[:, 1], label='Profile', color='black')
    for line in forest:
        ax.axvspan(line[1], line[2], facecolor='#2ca02c', alpha=0.15)
    # no forest energy lines
    ax.plot(profile[:, 0], profile[:, 1] + elh[:, 0], label='ELH No Forest')
    ax.fill_between(profile[:, 0], profile[:, 1] + elh[:, 1], profile[:, 1] + elh[:, 2], color='blue', alpha=.3)
    # forest energy lines
    ax.plot(profile[:, 0], profile[:, 1] + elh_forest[:, 0], label='ELH Forest', color='green')
    ax.fill_between(profile[:,0], profile[:,1] + elh_forest[:,1], profile[:,1] + elh_forest[:,2], color='green', alpha=.3)
    # right axis forest indicator
    ax1 = ax.twinx()
    ax1.plot(profile[:, 0], forest_indicator[:, 0], label='Forest Indicator', color='red')
    ax1.fill_between(profile[:, 0], forest_indicator[:, 1], forest_indicator[:, 2], color='red', alpha=.3)

    ax1.spines['right'].set_color('red')
    ax1.tick_params(axis='y', colors='red')
    ax1.yaxis.set_major_formatter(mtick.PercentFormatter())
    # ax1.set_ylim([0, 100])
    ax.set_xlabel('Distance [m]')
    ax.set_ylabel('Altitude [m]')
    ax2.set_title('FAT Results', y=1.2)
    for line in forest:
        ax2.axvspan(line[1], line[2], facecolor='#2ca02c', alpha=0.4)
    ax.grid()

    # ax1.set_yticks(np.linspace(0, ax1.get_ybound()[1]-5, 5))
    # ax.set_yticks(np.linspace(ax.get_ybound()[0]+5, ax.get_ybound()[1]-5, 4))

    fig.legend(loc='right', bbox_to_anchor=(0.8, 0.83), ncol=2)
    fig.tight_layout()
    fig.savefig('Energyline_withFI.png', dpi=300, bbox_inches="tight")
    plt.show()



def turin_results(profile, forest, elh, elh_forest, forest_indicator):
    fig, ax = plt.subplots()
    #profile
    ax.plot(profile[:, 0], profile[:, 1], label='Profile', color='black')
    for line in forest:
        ax.axvspan(line[1], line[2], facecolor='#2ca02c', alpha= .1*line[4])
    # no forest energy lines
    # ax1 = ax.twinx()
    # ax1.plot(profile[:, 0], forest_indicator[:, 0], label='Forest Indicator', color='red')
    # ax1.fill_between(profile[:, 0], forest_indicator[:, 1], forest_indicator[:, 2], color='red', alpha=.3)

    ax.plot(profile[:, 0], profile[:, 1] + elh[:, 0], label='ELH No Forest')
    ax.fill_between(profile[:, 0], profile[:, 1] + elh[:, 1], profile[:, 1] + elh[:, 2], color='blue', alpha=.3)
    # forest energy lines
    ax.plot(profile[:, 0], profile[:, 1] + elh_forest[:, 0], label='ELH Forest', color='green')
    ax.fill_between(profile[:,0], profile[:,1] + elh_forest[:,1], profile[:,1] + elh_forest[:,2], color='green', alpha=.3)
    ##right axis forest indicator
    ax1 = ax.twinx()
    ax1.plot(profile[:, 0], forest_indicator[:, 0], label='Forest Indicator', color='red')
    ax1.fill_between(profile[:, 0], forest_indicator[:, 1], forest_indicator[:, 2], color='red', alpha=.3)

    #ax1.spines['right'].set_color('red')
    #ax1.tick_params(axis='y', colors='red')
    #ax1.yaxis.set_major_formatter(mtick.PercentFormatter())
    # ax1.set_ylim([0, 100])
    ax.set_xlabel('Distance [m]')
    ax.set_ylabel('Altitude [m]')
    ax.set_title('FAT Results', y=1.2)


    # ax1.set_yticks(np.linspace(0, ax1.get_ybound()[1]-5, 5))
    # ax.set_yticks(np.linspace(ax.get_ybound()[0]+5, ax.get_ybound()[1]-5, 4))

    fig.legend(loc='right', bbox_to_anchor=(0.8, 0.83), ncol=2)
    fig.tight_layout()
    #path ="/home/W/DAmboise/Frei/Talks/turino_fat_talk/"
    #fig.savefig(path+'50_3000_elh.png', dpi=300, bbox_inches="tight")
    plt.show()
