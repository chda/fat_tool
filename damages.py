def total_damages( infra_linear, infra_buildings, avalanche_end,  slope_width, probability_someone_hit):
    """
    This function calculates the initial damages with out forest
    and damages accounting for different measures incluing forest
    """
    people_value = 4000000
    #lineara part
    damage_road = road_damage(infra_linear, slope_width, avalanche_end, people_value, probability_someone_hit)

    #buildings part
    damage_building = building_damages(infra_buildings,avalanche_end, people_value, probability_someone_hit)

    damage = damage_road + damage_building
    return damage



def road_damage(linear_infra, slope_width, avalanche_end, people_value, probability_someone_hit):
    """
    road_damage is not a good name for this function (is it road closure? )
    check to see if this function needs if elif or if indirect cost for all linear infra can be calculated in teh same way.
    type 1 = forest road 2= secondary road 3=primary road 4=highway 5=railway 6=powerline
    """
    value = {1: 100, 2:1200, 3:2400, 4:4000, 5:3000, 6:220} # euro/m
    car_value = {1: 0, 2:10000, 3:10000, 4:10000, 5:500000, 6:0} # euros
    number_people = {1: 0, 2:2, 3:2, 4:2, 5:40, 6:0} # number people per car, 5. trains 40 people is a train car at half capacity
    damage_road = 0  # initialize damage
    for line in linear_infra:
        x_location = line[1]
        road_type = line[2]
        infra_value = value[road_type]

        if (road_type) == 2 or (road_type) == 3 or (road_type) == 4: # if it is a car road
            traffic_intensity = line[3] # "number of cars hit"
        elif road_type == 5: # rail roads on car gets hit.
            traffic_intensity = 1
        else: # if we have forest road or power lines there is no people or cars
            traffic_intensity = 0

        deroute = line[4]

        total_people_value = people_value * number_people[road_type] * probability_someone_hit
        total_car_value = traffic_intensity * car_value[road_type] * probability_someone_hit # if people are hit so are cars
        total_infra_value = infra_value * slope_width
        damage_road = total_infra_value + total_people_value + total_car_value

        if x_location <= avalanche_end:
            damage_road += total_infra_value+ (traffic_intensity * car_value[road_type] + traffic_intensity * total_people_value) * probability_someone_hit

    return damage_road


def building_damages(infra_building, avalanche_end, people_value, probability_someone_hit):
    """
    calculates the building part of damages
    array vaues -> 1=nonresidential 2=residential 3=commercial 4=public
    """
    damage_buildings = 0
    for line in infra_building:  # loop over every line in infra_building.txt
        x_location = line[1]
        building_type = line[2]
        value = {1: 1200, 2: 1900, 3: 2600, 4: 2000}  #
        building_dimension = line[3]
        number_people = line[4]
        if x_location <= avalanche_end:
            damage_buildings += value[building_type] *building_dimension + number_people * people_value* probability_someone_hit

    return damage_buildings

def total_damages_dam(infra_linear, infra_buildings, avalanche_end,  slope_width, probability_someone_hit, dam_location):
    people_value = 4000000
    ###################linear ##################
    value = {1: 100, 2: 1200, 3: 2400, 4: 4000, 5: 3000, 6: 220}  # euro/m
    car_value = {1: 0, 2: 10000, 3: 10000, 4: 10000, 5: 500000, 6: 0}  # euros
    number_people = {1: 0, 2: 2, 3: 2, 4: 2, 5: 40,
                     6: 0}  # number people per car, 5. trains 40 people is a train car at half capacity
    damage_road = 0  # initialize damage
    dam_failure_prob = .01

    for line in infra_linear:
        x_location = line[1]
        road_type = line[2]
        infra_value = value[road_type]

        if (road_type) == 2 or (road_type) == 3 or (road_type) == 4:  # if it is a car road
            traffic_intensity = line[3]  # "number of cars hit"
        elif road_type == 5:  # rail roads on car gets hit.
            traffic_intensity = 1
        else:  # if we have forest road or power lines there is no people or cars
            traffic_intensity = 0

        deroute = line[4]

        total_people_value = people_value * number_people[road_type] * probability_someone_hit
        total_car_value = traffic_intensity * car_value[
            road_type] * probability_someone_hit  # if people are hit so are cars
        total_infra_value = infra_value * slope_width
        damage_road = total_infra_value + total_people_value + total_car_value

        if x_location <= avalanche_end:
            if x_location < dam_location:
                damage_road += total_infra_value + (traffic_intensity * car_value[road_type] + traffic_intensity * total_people_value) * probability_someone_hit
            else:
                damage_road += dam_failure_prob * (total_infra_value + (traffic_intensity * car_value[
                    road_type] + traffic_intensity * total_people_value) * probability_someone_hit)

    ########################### building part ###############

    damage_buildings = 0
    for line in infra_buildings:  # loop over every line in infra_building.txt
        x_location = line[1]
        building_type = line[2]
        value = {1: 1200, 2: 1900, 3: 2600, 4: 2000}  #
        building_dimension = line[3]
        number_people = line[4]


        if x_location <= avalanche_end:
            if x_location < dam_location: # if building located before dam
                damage_buildings += value[building_type] * building_dimension + number_people * people_value * probability_someone_hit
            else: #  buildings located after dam
                damage_buildings += dam_failure_prob * (value[building_type] * building_dimension + number_people * people_value * probability_someone_hit)

    damage = damage_road + damage_buildings
    return damage